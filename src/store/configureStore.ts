import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import notiReducer from 'app/components/Notification/slice/notificationSlice';
import departmentReducer from 'app/pages/AdmDepartmentPage/slice/departmentSlice';
import unitReducer from 'app/pages/AdmUnitPage/slice/unitSlice';
import authReducer from 'app/pages/AuthPage/slice/authSlice';
import departmentStaffReducer from 'app/pages/DepartmentDetail/DepartmentStaff/slice/departmentStaffSlice';
import planReducer from 'app/pages/DepartmentDetail/Plan/slice/planSlice';
import wareHouseReducer from 'app/pages/DepartmentWarehouse/slice/wareHouseSlice';
import supplierReducer from 'app/pages/SupplierPage/slice/supplierSlice';
import supplyReducer from 'app/pages/SupplyPage/slice/supplySlice';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    noti: notiReducer,
    supplier: supplierReducer,
    department: departmentReducer,
    supply: supplyReducer,
    plan: planReducer,
    departmentStaff: departmentStaffReducer,
    unit: unitReducer,
    wareHouse: wareHouseReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
