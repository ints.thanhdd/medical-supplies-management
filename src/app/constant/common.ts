export const UnitSupply = ['Lọ', 'Gam'];

export const typeOfSupply = [
  {
    label: 'Vật tư tiêu hao',
    value: 1,
  },
  {
    label: 'Dụng cụ y tế',
    value: 2,
  },
  {
    label: 'Hóa chất',
    value: 3,
  },
  {
    label: 'Sinh phẩm xét nghiệm',
    value: 4,
  },
];
