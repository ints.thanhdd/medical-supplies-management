import React from 'react';
import './Loading.scss';

type Props = {
  active: boolean;
  size?: number;
  type: 'fixed' | 'normal';
};
function Loading({ active, size = 12, type }: Props) {
  return (
    <div className={`loading-component ${type} ${active ? 'active' : ''} size${size}`}>
      <i className="fa-solid fa-spinner"></i>
    </div>
  );
}

export default Loading;
