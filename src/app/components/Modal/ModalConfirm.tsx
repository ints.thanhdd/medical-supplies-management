import React from 'react';
import Modal from './Modal';
import './Modal.scss';

type ModalConfirmProps = {
  isOpen: boolean;
  onClose: () => void;
  onConfirm: any;
  title: string;
  message?: string;
};
const ModalConfirm = ({ isOpen, onClose, title, message, onConfirm }: ModalConfirmProps) => {
  return (
    <Modal open={isOpen} onClose={onClose}>
      <div className="modal-title">{title}</div>
      <h1 className="modal-message">{message}</h1>
      <div className="modal-button-group">
        <button onClick={onClose} className="modal-button cancel">
          Cancel
        </button>
        <button onClick={onConfirm} className="modal-button confirm">
          Yes
        </button>
      </div>
    </Modal>
  );
};
export default ModalConfirm;
