import React, { ReactNode } from 'react';
import ReactDOM from 'react-dom';
import { CloseCircleOutlined } from '@ant-design/icons';

import './Modal.scss';
type ModalProps = {
  open: boolean;
  children: ReactNode;
  onClose: () => void;
};
const Modal = ({ children, onClose, open }: ModalProps) =>
  open
    ? ReactDOM.createPortal(
        <div className="wapper-modal">
          <div className="close-modal-btn" onClick={onClose}>
            <CloseCircleOutlined />
          </div>
          <div className="modal-container">{children}</div>
        </div>,
        document.body,
      )
    : null;
export default Modal;
