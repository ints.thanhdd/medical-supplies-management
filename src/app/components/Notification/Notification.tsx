import React, { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import './Notification.scss';
import { closeNoti, resetNoti } from './slice/notificationSlice';

const Notification = () => {
  const dispatch = useAppDispatch();
  const { active, type, title, desc, duration } = useAppSelector((state) => state.noti);
  const closeIcon = () => (
    <div
      className="notification-close-icon"
      onClick={() => {
        dispatch(resetNoti());
        dispatch(closeNoti());
      }}
    >
      <i className="fa-regular fa-circle-xmark"></i>
    </div>
  );
  const content = () => (
    <div className="notification-content">
      <p>{title}</p>
      <p>{desc}</p>
    </div>
  );
  useEffect(() => {
    return () => {
      dispatch(resetNoti());
    };
  }, []);

  useEffect(() => {
    let timeOut;
    if (active) {
      timeOut = setTimeout(() => {
        dispatch(closeNoti());
        dispatch(resetNoti());
      }, duration);
    }
    return () => clearTimeout(timeOut);
  }, [active, duration]);

  return (
    <>
      {(type === 'error' || !type) && (
        <div
          className={`notification-big error ${active === null ? '' : active ? 'show' : 'hide'}`}
        >
          {content()}
          {closeIcon()}
        </div>
      )}
      {(type === 'success' || !type) && (
        <div
          className={`notification-big success ${active === null ? '' : active ? 'show' : 'hide'}`}
        >
          {content()}
          {closeIcon()}
        </div>
      )}
      {(type === 'success-small' || !type) && (
        <div
          className={`notification-small success ${
            active === null ? '' : active ? 'show' : 'hide'
          }`}
        >
          {content()}
          {closeIcon()}
        </div>
      )}
      {(type === 'error-small' || !type) && (
        <div
          className={`notification-small error ${active === null ? '' : active ? 'show' : 'hide'}`}
        >
          {content()}
          {closeIcon()}
        </div>
      )}
    </>
  );
};

export default Notification;
