import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { notification } from 'antd';
type NotiClientState = {
  active?: boolean | null;
  type: 'success' | 'error' | 'success-small' | 'error-small' | null;
  duration: number;
  title: string;
  desc?: string;
};
const initialState: NotiClientState = {
  active: null,
  type: null,
  title: '',
  desc: '',
  duration: 0,
};
const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    showNoti: (state, action: PayloadAction<NotiClientState>) => {
      state.active = true;
      state.type = action.payload.type;
      state.title = action.payload.title;
      state.desc = action.payload.desc;
      state.duration = action.payload.duration;
    },
    closeNoti: (state) => {
      state.active = false;
      state.type = initialState.type;
      state.title = initialState.title;
      state.desc = initialState.desc;
      state.duration = initialState.duration;
    },
    resetNoti: (state) => {
      state.active = initialState.active;
      state.type = initialState.type;
      state.title = initialState.title;
      state.desc = initialState.desc;
      state.duration = initialState.duration;
    },
  },
  extraReducers: (builder) => {},
});

export const { actions, reducer: notiReducer } = slice;
export const { showNoti, closeNoti, resetNoti } = actions;
export default notiReducer;
