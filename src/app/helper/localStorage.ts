import { DEFAULT_LANGUAGE } from './../../locales/i18n';

export const getUserFromLocalStorage = () => {
  const dataLogin = JSON.parse(localStorage.getItem('dataLogin') ?? '') ?? '';
  return dataLogin;
};
export const getTokenFromLocalStorage = () => {
  const token = localStorage.getItem('accessToken') ?? '';
  return token;
};

export const getRefreshTokenFromLocalStorage = () => {
  const token = localStorage.getItem('refreshToken') ?? '';
  return token;
};

export const getLanguageFromLocalStorage = () => {
  const language = localStorage.getItem('language') ?? DEFAULT_LANGUAGE;
  return language;
};

export const saveTokenToLocalStorage = (token?: string) => {
  if (token) {
    localStorage.setItem('accessToken', token);
  }
};

export const saveRefreshTokenToLocalStorage = (refresh_token?: string) => {
  if (refresh_token) {
    localStorage.setItem('refreshToken', refresh_token);
  }
};
