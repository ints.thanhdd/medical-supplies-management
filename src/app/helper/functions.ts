import { typeOfSupply } from 'app/constant/common';
export const getPublicUrl = (name) => {
  return `${process.env.PUBLIC_URL}/img/${name}`;
};

// Convert type supplies
export const mappingTypeSupply = (type: string | number) => {
  return typeOfSupply.find((e) => e.value === Number(type))?.label ?? '';
};
