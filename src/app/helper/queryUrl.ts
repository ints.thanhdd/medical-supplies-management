import { IndexedObject } from 'app/types/common';
import queryString from 'query-string';
import { Location } from 'history';

export const isEmptyObject = (obj: IndexedObject) => {
  if (obj.constructor === Object && Object.keys(obj).length === 0) {
    return true;
  }
  return JSON.stringify(obj) === JSON.stringify({});
};

export const parseFloatNum = (str: string | (string | null)[] | null | number) => {
  const trimmed = str && typeof str === 'string' ? str.trim() : null;
  if (!trimmed) {
    return null;
  }
  const num = parseFloat(trimmed);
  const isFullyParsedNum = num.toString() === trimmed;
  return isFullyParsedNum ? num : null;
};

export const parseSearchParams = (search: string) => {
  const params = queryString.parse(search);
  return Object.keys(params).reduce((result: IndexedObject, key: string) => {
    const newResult = { ...result };
    const val = params[key];
    if (val === 'true') {
      newResult[key] = true;
    } else if (val === 'false') {
      newResult[key] = false;
    } else {
      const num = parseFloatNum(val);
      newResult[key] = num === null ? val : num;
    }
    return newResult;
  }, {});
};

export const createQueryUrl = (location: Location, params: IndexedObject) => {
  const { pathname, search } = location;
  const queryParams = parseSearchParams(search);
  const objQuery = { ...queryParams, ...params };
  if (isEmptyObject(params)) return pathname;

  const query = queryString.stringify(objQuery);
  return `${pathname}?${query}`;
};
