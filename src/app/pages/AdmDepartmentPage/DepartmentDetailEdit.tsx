import React, { useEffect, useState } from 'react';
import { Descriptions } from 'antd';
import { useHistory, useParams } from 'react-router-dom';
import { RollbackOutlined } from '@ant-design/icons';
import departmentApi from 'app/axios/api/departmentApi';
import { Department } from './slice/departmentType';

const DepartmentDetailEdit = () => {
  let { id } = useParams<{ id: string }>();
  const [dataDepartmentDetail, setDataDepartmentDetail] = useState<Department>({});

  useEffect(() => {
    departmentApi.getDepartmentDetail(id).then((res) => {
      setDataDepartmentDetail(res.data);
    });
  }, []);
  const history = useHistory();
  return (
    <div className="wapper-edit">
      <RollbackOutlined
        className="department-edit-icon"
        onClick={() => {
          history.goBack();
        }}
      />

      <Descriptions size="small" title="Thông tin khoa phòng" layout="horizontal" bordered>
        <Descriptions.Item contentStyle={{ fontSize: '16px' }} span={24} label="Tên khoa phòng">
          {dataDepartmentDetail.name}
        </Descriptions.Item>
        <Descriptions.Item contentStyle={{ fontSize: '15px' }} span={24} label="Tên trưởng phòng">
          Trần Văn Tiến
        </Descriptions.Item>
        <Descriptions.Item span={24} label="Telephone">
          {dataDepartmentDetail.phoneNumber}
        </Descriptions.Item>
        <Descriptions.Item span={24} label="Email">
          {dataDepartmentDetail.email}
        </Descriptions.Item>
        <Descriptions.Item span={24} label="Address">
          {dataDepartmentDetail.location}
        </Descriptions.Item>
        <Descriptions.Item span={24} label="Nhân viên">
          Trần Văn A
          <br />
          Trân Văn C
          <br />
          Trần Văn B
          <br />
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};
export default DepartmentDetailEdit;
