export type Department = {
    name?: string;
    location?: string;
    id?: string;
    email?: string;
    phoneNumber?: number;
   
  };
  export type InitialState = {
    loading: boolean;
    listDepartment: Array<Department>;
  };
  