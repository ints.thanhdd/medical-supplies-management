import { createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import departmentApi from 'app/axios/api/departmentApi';
import { InitialState } from './departmentType';

const initialState: InitialState = {
  loading: false,
  listDepartment: [],
};
export const getListDepartment = createAsyncThunk(
  '/department/getListSupplier',
  async (_, thunkAPI) => {
    const response = await departmentApi.getAllDepartment();
    return response.data;
  },
);
export const getSearchDepartment = createAsyncThunk(
  '/supplier/getSearchSupplier',
  async (param: any, thunkAPI) => {
    const response = await departmentApi.searchDepartment(param.name);
    return response.data;
  },
);

const slice = createSlice({
  name: 'supplier',
  initialState,
  reducers: {
    deleteDepartment: (state, action) => {
      const res = state.listDepartment.filter((re) => {
        return re.id !== action.payload.id;
      });
      state.listDepartment = res;
    },
    addDepartment: (state, action) => {
      state.listDepartment.push(action.payload);
    },
    patchDepartment: (state, action) => {
      const res = state.listDepartment.findIndex((re) => {
        return re.id === action.payload.id;
      });
      state.listDepartment[res] = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getListDepartment.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getListDepartment.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getListDepartment.fulfilled, (state, action: PayloadAction<Array<any>>) => {
      state.listDepartment = action.payload;
      state.loading = false;
    });
    builder.addCase(getSearchDepartment.fulfilled, (state, action: PayloadAction<Array<any>>) => {
      state.listDepartment = action.payload;
    });
  },
});

export const { actions, reducer: departmentReducer } = slice;
export const { deleteDepartment, addDepartment, patchDepartment } = actions;
export default departmentReducer;
