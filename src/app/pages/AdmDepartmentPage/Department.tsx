import React, { useEffect, useState } from 'react';
import { Button, Col, Row, Card, Skeleton } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { deleteDepartment, getListDepartment, getSearchDepartment } from './slice/departmentSlice';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import ModalConfirm from 'app/components/Modal/ModalConfirm';
import departmentApi from 'app/axios/api/departmentApi';
import ModalAddDepartment from './component/ModalAddDepartment';
import { useHistory } from 'react-router-dom';
import './Department.scss';
import { mPath } from 'app/constant/route';
import { showNoti } from 'app/components/Notification/slice/notificationSlice';

const { Meta } = Card;
const Deparment = () => {
  const history = useHistory();

  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [openModalAddDepartment, setOpenModalAddDepartment] = useState(false);

  const [infoDepartment, setInfoDepartment] = useState<any>({});
  const dispatch = useAppDispatch();
  const { listDepartment, loading } = useAppSelector((state) => state.department);

  useEffect(() => {
    dispatch(getListDepartment());
  }, []);

  const handleDeleteDepartment = () => {
    departmentApi.deleteDepartment(infoDepartment.id).then((res) => {
      dispatch(deleteDepartment(res.data));
      dispatch(
        showNoti({
          type: 'success-small',
          title: ' Delete success',
          duration: 2000,
        }),
      );
    });
    setOpenModalDelete(false);
  };

  const handleSearchDepartment = (e) => {
    dispatch(getSearchDepartment({ name: e.target.value }));
  };
  return (
    <div className="wapper-department">
      <ModalConfirm
        isOpen={openModalDelete}
        onConfirm={handleDeleteDepartment}
        onClose={() => {
          setOpenModalDelete(false);
        }}
        title="Are you sure to delete ?"
        message={infoDepartment.name}
      />
      <ModalAddDepartment
        isOpen={openModalAddDepartment}
        onClose={() => {
          setOpenModalAddDepartment(false);
        }}
      />

      <div className="deparment-header">
        <Row>
          <Col span={20}>
            <input
              onChange={handleSearchDepartment}
              className="department-header-input"
              placeholder="Search"
            />
          </Col>
          <Col span={4}>
            <Button
              onClick={() => {
                setOpenModalAddDepartment(true);
              }}
              className="btn-add"
            >
              ADD
            </Button>
          </Col>
        </Row>
      </div>
      <Row
        className="supplier-list"
        gutter={[20, 20]}
        wrap={true}
        style={{ marginLeft: 0, marginRight: 0 }}
      >
        {listDepartment?.map((department) => (
          <Col key={department.id} span={8}>
            <div className="department-item">
              <Card
                hoverable
                style={{ border: '1px solid #494646' }}
                actions={[
                  <EditOutlined
                    onClick={() => {
                      history.push(mPath.A_DEPARTMENT_DETAIL.replace(':id', department.id ?? ''));
                    }}
                    key="edit"
                  />,
                  <DeleteOutlined
                    onClick={() => {
                      setOpenModalDelete(true);
                      setInfoDepartment(department);
                    }}
                  />,
                ]}
              >
                <Skeleton className="department-item-sub" loading={loading} avatar active>
                  <Meta title={department.name} description={department.location} />
                </Skeleton>
              </Card>
            </div>
          </Col>
        ))}
      </Row>

      <div></div>
    </div>
  );
};
export default Deparment;
