import { Button, Col, Form, Input, Row } from 'antd';
import Upload from 'antd/lib/upload/Upload';
import departmentApi from 'app/axios/api/departmentApi';
import Loading from 'app/components/Loading/Loading';
import Modal from 'app/components/Modal/Modal';
import React, { useState } from 'react';
import { useAppDispatch } from 'store/hooks';
import { patchDepartment } from '../slice/departmentSlice';
import { Department } from '../slice/departmentType';
import './ModalEditDepartment.scss';
type Props = {
  isOpen: boolean;
  onClose: () => void;
  infoDepartment: Department;
};

const ModalEditDepartment = ({ isOpen, onClose, infoDepartment }: Props) => {
  const dispatch = useAppDispatch();
  const [listDepartment, setListDepartment] = useState({
    id: infoDepartment.id,
    name: infoDepartment.name,
    location: infoDepartment.location,
  });
  const onFinish = (values: any) => {
    values.id = infoDepartment.id;
    departmentApi.patchDepartment(values).then((res) => {
      dispatch(patchDepartment(res.data));
      onClose();
    });
  };

  return (
    <Modal open={isOpen} onClose={onClose}>
      <div className="department-wapper">
        <h1 className="department-title">Chỉnh sửa khoa phòng</h1>
        <div className="department-form-edit">
          <Form
            name="basic"
            labelCol={{ span: 0 }}
            wrapperCol={{ span: 24 }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              name="name"
              initialValue={listDepartment.name}
              rules={[{ required: true, message: 'Please input department name' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="location"
              initialValue={listDepartment.location}
              rules={[{ required: true, message: 'Please input department location' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 10, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Update
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </Modal>
  );
};
export default ModalEditDepartment;
