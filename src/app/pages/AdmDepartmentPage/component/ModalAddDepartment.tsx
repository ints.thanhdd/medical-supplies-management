import { Button, Form, Input } from 'antd';
import departmentApi from 'app/axios/api/departmentApi';
import Modal from 'app/components/Modal/Modal';
import { showNoti } from 'app/components/Notification/slice/notificationSlice';
import React from 'react';
import { useAppDispatch } from 'store/hooks';
import { addDepartment } from '../slice/departmentSlice';
import './ModalEditDepartment.scss';
type Props = {
  isOpen: boolean;
  onClose: () => void;
};

const ModalAddDepartment = ({ isOpen, onClose }: Props) => {
  const dispatch = useAppDispatch();

  const onFinish = (values: any) => {
    departmentApi.createDepartment(values).then((res) => {
      dispatch(addDepartment(res.data));
      dispatch(
        showNoti({
          type: 'success-small',
          title: 'Create success',
          duration: 2000,
        }),
      );
    });
    onClose();
  };

  return (
    <Modal open={isOpen} onClose={onClose}>
      <div className="department-wapper">
        <h1 className="department-title">Thêm mới khoa phòng</h1>
        <div className="department-form-edit">
          <Form
            name="basic"
            labelCol={{ span: 0 }}
            wrapperCol={{ span: 24 }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              name="name"
              rules={[{ min: 3, required: true, message: 'Please input departmentname!' }]}
            >
              <Input placeholder="Name" />
            </Form.Item>

            <Form.Item
              name="location"
              rules={[{ min: 3, required: true, message: 'Please input your localtion!' }]}
            >
              <Input placeholder="Location" />
            </Form.Item>
            <Form.Item
              name="phoneNumber"
              rules={[{ required: true, message: 'Please input your phonenumber!' }]}
            >
              <Input placeholder="Phonenumber" />
            </Form.Item>
            <Form.Item
              name="email"
              rules={[{ required: true, message: 'Please input your email!' }]}
            >
              <Input placeholder="Email" />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 10, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </Modal>
  );
};
export default ModalAddDepartment;
