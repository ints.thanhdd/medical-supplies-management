import { createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import storeDepartmentApi from 'app/axios/api/storeDepartmentApi';
import { InitialState } from './wareHouseType';

const initialState: InitialState = {
  loading: false,
  dataWareHouse: {},
};
export const getDataWareHouse = createAsyncThunk(
  '/wareHouse/getDataWareHouse',
  async (query: any, _) => {
    const response = await storeDepartmentApi.getDataStoreInDepartment(query);
    return response.data;
  },
);

const slice = createSlice({
  name: 'supply',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getDataWareHouse.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getDataWareHouse.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getDataWareHouse.fulfilled, (state, action: PayloadAction<any>) => {
      state.dataWareHouse = action.payload;
    });
  },
});

export const { actions, reducer: wareHouseReducer } = slice;
export default wareHouseReducer;
