import React, { useEffect, useState } from 'react';
import { Table, Divider, Popconfirm, Pagination, Row, Select } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import supplyApi from 'app/axios/api/supplyApi';
import { useLocation, useHistory } from 'react-router-dom';
import { createQueryUrl, parseSearchParams } from 'app/helper/queryUrl';
import { dangerLever } from 'app/constant/dangerLevel';
import { useTranslation } from 'react-i18next';
import { translations } from 'locales/translations';
import { typeOfSupply } from 'app/constant/common';
import { deleteSupply, getListSupply } from '../SupplyPage/slice/supplySlice';
import { getDataWareHouse } from './slice/wareHouseSlice';
import { getUserFromLocalStorage } from 'app/helper/localStorage';

interface DataType {
  name?: string;
  type?: string;
  unit?: string;
  quantity?: number;
  dengerLevel?: number;
  id?: any;
  key?: string;
}

const DepartmentWarehouse: React.FC = () => {
  const dispatch = useAppDispatch();
  const location = useLocation();
  const history = useHistory();
  const { t } = useTranslation();
  const { sortBy, dangerLevel, type } = parseSearchParams(location.search);
  const { listSupply, total } = useAppSelector((state) => state.supply);
  const { dataWareHouse, loading } = useAppSelector((state) => state.wareHouse);
  console.log(dataWareHouse);
  const userData = useAppSelector((state) => state.auth).user ?? getUserFromLocalStorage();
  console.log(userData);

  const [curPage, setCurPage] = useState(1);
  useEffect(() => {
    dispatch(
      getDataWareHouse({ page: curPage, sortBy, dangerLevel, type, id: userData.department }),
    );
  }, [curPage, sortBy, dangerLevel, type, dispatch]);

  const columns: ColumnsType<DataType> = [
    {
      title: t(translations.SUPPLY_LIST.SUPPLY_NAME),
      dataIndex: 'name',
      width: '35%',
    },
    {
      title: t(translations.COMMON.TYPE),
      dataIndex: 'type',
      width: '20%',
    },
    {
      title: t(translations.COMMON.UNIT),
      dataIndex: 'unit',
      width: '20%',
    },
    {
      title: t(translations.COMMON.DANGER_LEVEL),
      dataIndex: 'dangerLevel',
      width: '10%',
    },
    {
      title: '',
      dataIndex: 'action',
      width: '15%',
      render: (_, data) =>
        dataSupply && dataSupply?.length >= 1 ? (
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(data.id)}>
            <a>Delete</a>
          </Popconfirm>
        ) : null,
    },
  ];

  let dataSupply = listSupply?.map((list, index) => {
    return { ...list, key: String(index) };
  });
  const handleDelete = (id) => {
    supplyApi.deleteSupply(id).then((res) => {
      dispatch(deleteSupply(res.data));
    });
  };
  const onChangePage = (e: number) => {
    setCurPage(e);
    history.replace(createQueryUrl(location, { page: e }));
  };
  const onSort = (data: string) => {
    history.replace(createQueryUrl(location, { sortBy: data === 'new' ? 'desc' : 'asc' }));
  };
  const onFilterDangerLevel = (e) => {
    setCurPage(1);
    history.replace(createQueryUrl(location, { dangerLevel: e }));
  };
  const onFilterType = (e) => {
    setCurPage(1);
    history.replace(createQueryUrl(location, { type: e }));
  };
  const onClearFilter = () => {
    history.replace(location.pathname);
  };

  return (
    <div className="wapper-listsuply">
      <Divider style={{ fontSize: '24px', fontWeight: '600' }}>
        {t(translations.SUPPLY_LIST.LIST_SUPPLY)}
      </Divider>
      <Row justify="space-between">
        <div className="sort-container">
          <div className="sort-wrapper">
            <div
              className={`sort-item ${sortBy === 'desc' ? 'active' : ''}`}
              onClick={() => onSort('new')}
            >
              <i className="fa-solid fa-arrow-up-wide-short"></i>
              <p>Lastest</p>
            </div>
            <div
              className={`sort-item ${sortBy === 'asc' ? 'active' : ''}`}
              onClick={() => onSort('old')}
            >
              <i className="fa-solid fa-arrow-down-wide-short"></i>
              <p>Oldest</p>
            </div>
          </div>
          <div className="sort-wrapper">
            <p>Danger Level</p>
            <Select
              placeholder="Danger level"
              style={{ width: 120 }}
              onChange={onFilterDangerLevel}
              options={dangerLever.map((e) => ({
                value: e,
                label: e,
              }))}
              value={dangerLevel}
            />
          </div>
          <div className="sort-wrapper">
            <p>Type</p>
            <Select
              placeholder="Type"
              style={{ width: 120 }}
              onChange={onFilterType}
              options={typeOfSupply.map((e) => ({
                value: e.label,
                label: e.label,
              }))}
              value={type}
            />
          </div>
        </div>

        <div className="clear-filter" onClick={onClearFilter}>
          <p>Clear Filter</p>
          <i className="fa-solid fa-eraser"></i>
        </div>
      </Row>
      <div className="table-supply">
        <Table
          loading={loading}
          columns={columns}
          dataSource={dataSupply}
          size="middle"
          pagination={false}
        />
      </div>
      <Row justify="center">
        <Pagination defaultCurrent={1} current={curPage} total={total} onChange={onChangePage} />
      </Row>
    </div>
  );
};

export default DepartmentWarehouse;
