// import * as React from 'react';
import { Col, Row } from 'antd';
import { Button, Space } from 'antd';
import './Supplier.scss';
import { DeleteOutlined } from '@ant-design/icons';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import React, { useEffect, useState } from 'react';
import { deleteSupplier, getListSupplier, getSearchSupplier } from './slice/supplierSlice';
import supplierApi from 'app/axios/api/supplierApi';
import { SupplierType } from './slice/supplierType';
import ModalConfirm from 'app/components/Modal/ModalConfirm';
import Modal from 'app/components/Modal/Modal';
import ModalAddSupplier from './component/ModalAddSupplier';
import ModalSupplierDetail from './component/ModalSupplierDetail';
const Supplier = () => {
  const dispatch = useAppDispatch();
  const { listSupplier } = useAppSelector((state) => state.supplier);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [openModalAddSupplier, setOpenModalAddSupplier] = useState(false);
  const [openModalSupplierDetail, setOpenModalSupplierDetail] = useState(false);
  const [infoSupplier, setInfoSupplier] = useState<SupplierType>({});

  useEffect(() => {
    dispatch(getListSupplier());
  }, []);
  const handleDeleteSupplier = () => {
    supplierApi.deleteSupplier(infoSupplier.id).then((res) => {
      dispatch(deleteSupplier(res.data));
    });

    // dispatch(getListSupplier());
    setOpenModalDelete(false);
  };
  const handleSearchSupplier = (e) => {
    dispatch(getSearchSupplier({ name: e.target.value }));
  };
  return (
    <div className="wapper-supplier">
      <ModalConfirm
        isOpen={openModalDelete}
        onConfirm={handleDeleteSupplier}
        onClose={() => {
          setOpenModalDelete(false);
        }}
        title="Are you sure to delete ?"
        message={infoSupplier.name}
      />
      <ModalAddSupplier
        isOpen={openModalAddSupplier}
        onClose={() => {
          setOpenModalAddSupplier(false);
        }}
      />
      {openModalSupplierDetail && (
        <ModalSupplierDetail
          isOpen={openModalSupplierDetail}
          onClose={() => {
            setOpenModalSupplierDetail(false);
          }}
          supplierDetail={infoSupplier}
        />
      )}
      <div className="supplier-header">
        <Row>
          <Col span={20}>
            <input
              onChange={handleSearchSupplier}
              className="supplier-header-input"
              placeholder="search"
            />
          </Col>
          <Col span={4}>
            <Button
              onClick={() => {
                setOpenModalAddSupplier(true);
              }}
              className="btn-add"
              // type="primary"
            >
              ADD
            </Button>
          </Col>
        </Row>
      </div>
      <Row
        className="supplier-list"
        gutter={[50, 30]}
        wrap={true}
        style={{ marginLeft: 0, marginRight: 0 }}
      >
        {listSupplier?.map((supplier) => (
          <Col key={supplier.id} span={24}>
            <div className="supplier-iteam">
              <div
                onClick={() => {
                  setOpenModalDelete(true);
                  setInfoSupplier(supplier);
                }}
              >
                <DeleteOutlined className="supplier-iteam-icon-close" />
              </div>
              <div
                onClick={() => {
                  setOpenModalSupplierDetail(true);
                  setInfoSupplier(supplier);
                }}
                className="supplier-iteam-info"
              >
                <div className="supplier-iteam-img">
                  <img
                    className="supplier-iteam-img-logo"
                    src="http://themco.com.vn/wp-content/uploads/2018/05/resize_536x402.jpg"
                    alt=""
                  />
                </div>
                <div className="supplier-iteam-info">
                  <h4 className="supplier-iteam-info-name">{supplier.name}</h4>
                  <h4 className="supplier-iteam-info-adress">{supplier.address}</h4>
                </div>
              </div>
            </div>
          </Col>
        ))}
      </Row>
      <div></div>
    </div>
  );
};
export default Supplier;
