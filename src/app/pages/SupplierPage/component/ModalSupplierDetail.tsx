import { Button, Col, Form, Input, Row } from 'antd';
import supplierApi from 'app/axios/api/supplierApi';
import Loading from 'app/components/Loading/Loading';
import Modal from 'app/components/Modal/Modal';
import React, { useState } from 'react';
import { useAppDispatch } from 'store/hooks';
import { patchSupplier } from '../slice/supplierSlice';
import { SupplierType } from '../slice/supplierType';
import './ModalAddSupplier.scss';
type ModalSupplierDetail = {
  isOpen: boolean;
  onClose: () => void;
  supplierDetail: SupplierType;
};

const layout = {
  labelCol: { span: 0 },
  wrapperCol: { span: 22 },
};
const tailLayout = {
  wrapperCol: { offset: 0, span: 16 },
};
const ModalSupplierDetail = ({ isOpen, onClose, supplierDetail }: ModalSupplierDetail) => {
  const [loading, setLoading] = useState(false);

  const dispatch = useAppDispatch();
  const [openEditSupplier, setOpenEditSupplier] = useState(false);

  const [listSupplierEdit, setListSupplierEdit] = useState({
    id: supplierDetail.id,
    name: supplierDetail.name,
    address: supplierDetail.address,
    email: supplierDetail.email,
    phone: supplierDetail.phone,
  });
  const [form] = Form.useForm();
  const onFinish = (values: any) => {
    setLoading(true);
    values.id = supplierDetail.id;
    supplierApi.patchSupplier(values).then((res) => {
      dispatch(patchSupplier(res.data));
      onClose();
    });
  };
  const handleEditSupplier = () => {
    setOpenEditSupplier(!openEditSupplier);
  };

  return (
    <Modal open={isOpen} onClose={onClose}>
      <div className="wapper-supplier-detail">
        <Row className="container-supplier-detail">
          <Col span={14}>
            <Row>
              {openEditSupplier ? (
                <Col span={20}>
                  <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
                    <Form.Item
                      name="name"
                      initialValue={listSupplierEdit.name}
                      rules={[{ required: true }]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      name="address"
                      initialValue={listSupplierEdit.address}
                      rules={[{ required: true }]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      name="email"
                      initialValue={listSupplierEdit.email}
                      rules={[{ required: true }]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      name="phone"
                      initialValue={listSupplierEdit.phone}
                      rules={[{ required: true }]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item {...tailLayout}>
                      <Button type="primary" htmlType="submit">
                        {!loading && 'Update'}
                        <Loading active={loading} type="normal" size={22} />
                      </Button>
                    </Form.Item>
                  </Form>
                </Col>
              ) : (
                <Col span={20}>
                  <Row className="supplier-detail-name">
                    <Col span={20}>
                      <div>{supplierDetail.name}</div>
                    </Col>
                  </Row>
                  <Row className="supplier-detail-address">
                    <Col span={20}>
                      <div>{supplierDetail.address}</div>
                    </Col>
                  </Row>
                  <Row className="supplier-detail-email">
                    <Col span={20}>
                      <div>{supplierDetail.email}</div>
                    </Col>
                  </Row>
                  <Row className="supplier-detail-phone">
                    <Col span={20}>
                      <div>{supplierDetail.phone}</div>
                    </Col>
                  </Row>
                </Col>
              )}

              <Col span={4}>
                <Button
                  onClick={handleEditSupplier}
                  className="supplier-detail-btn-edit"
                  type="primary"
                  danger
                >
                  Edit
                </Button>
              </Col>
            </Row>
          </Col>
          <Col span={10}>
            <div className="supplier-detail-img">
              <img
                src="http://themco.com.vn/wp-content/uploads/2018/05/resize_536x402.jpg"
                alt=""
              />
            </div>
          </Col>
        </Row>
      </div>
    </Modal>
  );
};
export default ModalSupplierDetail;
