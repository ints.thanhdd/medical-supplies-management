import Modal from 'app/components/Modal/Modal';
import React from 'react';
import { Upload, Form, Input, Select, Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import './ModalAddSupplier.scss';
import { useAppDispatch } from 'store/hooks';
// import { createSupplier } from '../slice/supplierSlice';
import supplierApi from 'app/axios/api/supplierApi';
import { addSupplier, getListSupplier } from '../slice/supplierSlice';
type ModalAppSupplier = {
  isOpen: boolean;
  onClose: () => void;
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const layout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 19 },
};
const ModalAddSupplier = ({ isOpen, onClose }: ModalAppSupplier) => {
  const dispatch = useAppDispatch();
  const [form] = Form.useForm();

  const onFinish = (values: any) => {
    supplierApi.createSupplier(values).then((res) => {
      console.log(res.data);
      dispatch(addSupplier(res.data));
    });
    onClose();
  };

  const onReset = () => {
    form.resetFields();
  };

  return (
    <Modal open={isOpen} onClose={onClose}>
      <div className="wapper-modal-add-supplier">
        <div className="modal-add-supplier-title">Nhập nhà cung cấp</div>
        <Form
          {...layout}
          form={form}
          className="modal-add-supplier-form"
          name="control-hooks"
          onFinish={onFinish}
        >
          <Form.Item name="name" label="Name" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="email" label="Email" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="address" label="Address" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="phone" label="Phone" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Upload" valuePropName="fileList">
            <Upload action="/upload.do" listType="picture-card">
              <div>
                <PlusOutlined />
                <div style={{ marginTop: 8 }}>Upload</div>
              </div>
            </Upload>
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button htmlType="button" onClick={onReset}>
              Reset
            </Button>
          </Form.Item>
        </Form>
      </div>
    </Modal>
  );
};
export default ModalAddSupplier;
