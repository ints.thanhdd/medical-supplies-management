import { createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import supplierApi from 'app/axios/api/supplierApi';
import { InitialState } from './supplierType';

const initialState: InitialState  = {
    loading: false,
    listSupplier: [],
};
export const getListSupplier = createAsyncThunk(
    '/supplier/getListSupplier',
    async (_, thunkAPI) => {
      const response = await supplierApi.getAllSupplier();
      return response.data;
    },
  );
export const getSearchSupplier = createAsyncThunk(
    '/supplier/getSearchSupplier',
    async (param: any, thunkAPI) => {
      const response = await supplierApi.searchSupplier(param.name);
      return response.data;
    },
  );
  // export const createSupplier= createAsyncThunk(
  //   '/supplier/postSupplier',
  //   async (param: any, thunkAPI) => {
  //     const response = await supplierApi.createSupplier(param);
  //     return response.data;

  //   },
  // );
const slice = createSlice({
  name: 'supplier',
  initialState,
  reducers: {
    deleteSupplier: (state, action) => {
      const res = state.listSupplier.filter((re) => {
          return re.id !== action.payload.id
        
      })
      // console.log(state.listSupplier)
      state.listSupplier = res;
    },
    addSupplier: (state, action) => {
      state.listSupplier.push(action.payload)
    },
    patchSupplier: (state, action) => {
      const res = state.listSupplier.findIndex((re) => {
        return re.id == action.payload.id
      
    })
    state.listSupplier[res] = action.payload
    }
  },
  extraReducers: (builder) => {
    builder.addCase(getListSupplier.pending, (state) => {
        state.loading = true;
      });
      builder.addCase(getListSupplier.rejected, (state) => {
        state.loading = false;
      });
      builder.addCase(
        getListSupplier.fulfilled,
        (state, action: PayloadAction<Array<any>>) => {
          state.listSupplier = action.payload;
        },
        
      );
      builder.addCase(
        getSearchSupplier.fulfilled,
        (state, action: PayloadAction<Array<any>>) => {
          state.listSupplier = action.payload;
          console.log(action.payload)
        },
        
      );
      
      
  },
});

export const { actions, reducer: supplierReducer } = slice;
export const {deleteSupplier, addSupplier,patchSupplier} = actions
export default supplierReducer;
