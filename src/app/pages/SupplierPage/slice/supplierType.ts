export type SupplierType = {
  name?: string;
  address?: string;
  phone?: string;
  email?: string;
  id?: any;
};
export type InitialState = {
  loading: boolean;
  listSupplier: Array<SupplierType>;
};
