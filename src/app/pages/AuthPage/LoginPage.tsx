import { Button, Form, Input } from 'antd';
import React, { useState } from 'react';
import './AuthPage.scss';
import { Link, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { translations } from 'locales/translations';
import { LoginForm } from './slice/authType';
import { loginUser } from './slice/authSlice';
import { useAppDispatch } from 'store/hooks';
import authApi from 'app/axios/api/authApi';
import { showNoti } from 'app/components/Notification/slice/notificationSlice';
import { HTTP_STATUS } from 'app/helper/constant';
import Loading from 'app/components/Loading/Loading';
import { mPath } from 'app/constant/route';

const LoginPage: React.FC = () => {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const history = useHistory();
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(false);
  const handleLogin = async (values: LoginForm) => {
    setLoading(true);
    await authApi
      .loginUser(values)
      .then((res: any) => {
        if (res.status === HTTP_STATUS.OK) {
          const {
            data: { accessToken, refreshToken, ...userData },
          } = res;
          dispatch(loginUser(userData));
          localStorage.setItem('dataLogin', JSON.stringify(userData));
          form.resetFields();
          localStorage.setItem('accessToken', accessToken);
          localStorage.setItem('refreshToken', refreshToken);

          if (userData.role === 'department') {
            history.push(mPath.M_HOME);
          } else {
            history.push(mPath.A_HOME);
          }
          dispatch(
            showNoti({
              type: 'success-small',
              title: 'Hello, Welcomeback ',
              duration: 3000,
            }),
          );
        }
      })
      .catch((err) =>
        dispatch(
          showNoti({
            type: 'error',
            title: 'Opp!, Something went wrong ',
            desc: err.response.data,
            duration: 3000,
          }),
        ),
      )
      .finally(() => setLoading(false));
  };

  return (
    <div className="login-wrapper">
      <div className="login-form">
        <h2 style={{ fontSize: '1.6rem', fontWeight: '600', margin: '20px 0' }}>
          {t(translations.AUTH.LOGIN_TITLE)}
        </h2>
        <Form
          form={form}
          name="basic"
          wrapperCol={{ span: 30 }}
          initialValues={{ remember: true }}
          onFinish={handleLogin}
          autoComplete="on"
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: t(translations.AUTH.USERNAME_INPUT_REQUIRED) }]}
          >
            <Input placeholder={t(translations.AUTH.USERNAME_INPUT_PLACEHOLDER)} />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[{ required: true, message: t(translations.AUTH.PASSWORD_INPUT_REQUIRED) }]}
          >
            <Input.Password placeholder={t(translations.AUTH.PASSWORD_INPUT_PLACEHOLDER)} />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 0, span: 30 }}>
            <Button className="button-submit" type="primary" htmlType="submit">
              {!loading && t(translations.AUTH.SIGN_IN)}
              <Loading active={loading} type="normal" size={22} />
            </Button>
          </Form.Item>
        </Form>
        <div className="sign-in">
          <span>
            {t(translations.AUTH.SUGGEST_USER_REGISTER)}
            <Link to="/register"> {t(translations.AUTH.SIGN_UP)}</Link>
          </span>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
