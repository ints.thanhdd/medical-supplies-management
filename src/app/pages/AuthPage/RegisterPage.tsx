import { Button, Form, Input, Radio, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import './AuthPage.scss';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { Link, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import authApi from 'app/axios/api/authApi';
import { HTTP_STATUS } from 'app/helper/constant';
import { showNoti } from 'app/components/Notification/slice/notificationSlice';
import { translations } from 'locales/translations';
import Loading from 'app/components/Loading/Loading';
import { getListDepartment } from '../AdmDepartmentPage/slice/departmentSlice';

const SECREC_KEY = `123`;

const RegisterPage: React.FC = () => {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const history = useHistory();
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(false);
  const { listDepartment } = useAppSelector((state) => state.department);
  const [isManagement, setIsManagement] = useState('');
  useEffect(() => {
    dispatch(getListDepartment());
  }, []);
  const handleRegisterUser = async (values) => {
    const value = {
      name: values.name,
      username: values.username,
      email: values.email,
      password: values.password,
      role: values.role,
      department: values.department,
    };
    if (values.code !== SECREC_KEY) {
      dispatch(
        showNoti({
          type: 'error-small',
          title: 'PIN Code is not valid',
          duration: 3000,
        }),
      );
      return;
    }
    setLoading(true);
    await authApi.registerUser(value).then((res: any) => {
      if (res.status === HTTP_STATUS.OK) {
        form.resetFields();
        history.push('/login');
        dispatch(
          showNoti({
            type: 'success-small',
            title: 'Register Success',
            duration: 3000,
          }),
        );
      } else {
        dispatch(
          showNoti({
            type: 'error',
            title: 'Opp!, Something went wrong ',
            desc: res.response.data,
            duration: 3000,
          }),
        );
      }
      setLoading(false);
    });
  };
  return (
    <div className="login-wrapper">
      <div className="login-form">
        <h2 style={{ fontSize: '1.6rem', fontWeight: '600', margin: '20px 0' }}>
          {t(translations.AUTH.REGISTER_TITLE)}
        </h2>
        <Form
          form={form}
          name="basic"
          size="small"
          initialValues={{ remember: true }}
          onFinish={handleRegisterUser}
          autoComplete="off"
        >
          <Form.Item
            name="name"
            rules={[
              {
                type: 'string',
                required: true,
                message: 'Please input your nickname!',
                whitespace: true,
              },
              { min: 6, max: 20, message: 'Username is at least 6 characters' },
            ]}
          >
            <Input placeholder="Name" />
          </Form.Item>
          <Form.Item
            name="username"
            rules={[
              {
                type: 'string',
                required: true,
                message: 'Please input your nickname!',
                whitespace: true,
              },
              { min: 6, max: 20, message: 'Username is at least 6 characters' },
            ]}
          >
            <Input placeholder="Usename" />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              {
                type: 'email',
                message: 'Email format is not valid',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ]}
          >
            <Input placeholder="Email" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password placeholder="Password" />
          </Form.Item>

          <Form.Item
            name="confirm"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error('The two passwords that you entered do not match!'),
                  );
                },
              }),
            ]}
          >
            <Input.Password placeholder="Confirm Password" />
          </Form.Item>
          <Form.Item name="role">
            <Radio.Group onChange={(e) => setIsManagement(e.target.value)}>
              <Radio value="management">Management</Radio>
              <Radio value="department">Department</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item name="department">
            <Select
              placeholder="Department"
              disabled={isManagement === 'department' ? false : true}
            >
              {listDepartment.map((department) => (
                <Select.Option key={department.id} value={department.id}>
                  {department.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name="code"
            rules={[
              {
                required: true,
                message: 'Please input PIN code!',
              },
            ]}
          >
            <Input placeholder="PIN Code" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" className="button-submit">
              {!loading && t(translations.AUTH.SIGN_UP)}
              <Loading active={loading} type="normal" size={22} />
            </Button>
          </Form.Item>
        </Form>
        <div className="sign-in">
          <span>
            {t(translations.AUTH.SUGGEST_USER_LOGIN)}
            <Link to="/login"> {t(translations.AUTH.SIGN_IN)}</Link>
          </span>
        </div>
      </div>
    </div>
  );
};

export default RegisterPage;
