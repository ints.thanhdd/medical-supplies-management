import { IndexedObject } from 'app/types/common';
export type LoginForm = {
  username: string;
  password: string;
};

export type TInitialState = {
  loading: boolean;
  user: IndexedObject | null;
  isAuthenticated: boolean;
  error: string;
};
