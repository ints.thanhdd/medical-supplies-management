import { createSlice } from '@reduxjs/toolkit';
import { TInitialState } from './authType';

const initialState: TInitialState = {
  loading: false,
  user: null,
  isAuthenticated: false,
  error: '',
};

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    loginUser: (state, action) => {
      state.user = action.payload;
      state.isAuthenticated = true;
    },
    logoutUser: (state, action) => {
      localStorage.clear();
      state.user = null;
      state.isAuthenticated = false;
    },
  },
  extraReducers: (builder) => {},
});

export const { actions, reducer: authReducer } = slice;
export const { loginUser, logoutUser } = actions;
export default authReducer;
