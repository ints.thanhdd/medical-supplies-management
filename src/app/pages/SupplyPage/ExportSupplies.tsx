import { Button, Form, Input, Select, Col, Row } from 'antd';
import './supplyPage.scss';
import React from 'react';

const { Option } = Select;
const ExportSupplies: React.FC = () => {
  const [form] = Form.useForm();

  const onGenderChange = (value: string) => {
    switch (value) {
      case 'male':
        form.setFieldsValue({ note: 'Hi, man!' });
        return;
      case 'female':
        form.setFieldsValue({ note: 'Hi, lady!' });
        return;
      case 'other':
        form.setFieldsValue({ note: 'Hi there!' });
    }
  };

  const onFinish = (values: any) => {
  };

  const onReset = () => {
    form.resetFields();
  };

  return (
    <Form
      className="form-supply-wapper"
      layout="vertical"
      form={form}
      name="control-hooks"
      onFinish={onFinish}
    >
      <div className="form-supply-body">
        <Row>
          <Col span={24}>Nhập thông tin vật tư</Col>
        </Row>
        <Row>
          <Col span={24}>
            <Form.Item name="tên vật tư" label="Tên vật tư" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          <Col span={12}>
            <Form.Item name="Nhóm vật tư" label="Nhóm vật tư" rules={[{ required: true }]}>
              <Select placeholder="Chọn nhóm vật tư" onChange={onGenderChange} allowClear>
                <Option value="male">male</Option>
                <Option value="female">female</Option>
                <Option value="other">other</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="Khoa-Phòng" label="Khoa-Phòng" rules={[{ required: true }]}>
              <Select placeholder="Chọn khoa phòng" onChange={onGenderChange} allowClear>
                <Option value="male">male</Option>
                <Option value="female">female</Option>
                <Option value="other">other</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          <Col span={8}>
            <Form.Item name="Số lượng" label="Số lượng" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item name="Đơn vị" label="Đơn vị" rules={[{ required: true }]}>
              <Select placeholder="Chọn đơn vị tính" onChange={onGenderChange} allowClear>
                <Option value="male">male</Option>
                <Option value="female">female</Option>
                <Option value="other">other</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row justify="end">
          <Col span={8}>
            <Form.Item>
              <Button style={{ margin: 10 }} type="primary" htmlType="submit">
                Submit
              </Button>
              <Button htmlType="button" onClick={onReset}>
                Reset
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </div>
    </Form>
  );
};

export default ExportSupplies;
