import { Button, Form, Input, Select, Col, Row } from 'antd';
import './supplyPage.scss';
import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { getListSupplier } from '../SupplierPage/slice/supplierSlice';
import supplyApi from 'app/axios/api/supplyApi';
import { addSupply, getListSupply } from './slice/supplySlice';
import { dangerLever } from 'app/constant/dangerLevel';
import { getUnit } from '../AdmUnitPage/slice/unitSlice';
import { useHistory } from 'react-router-dom';
import { mPath } from 'app/constant/route';
import { typeOfSupply } from 'app/constant/common';
import unitApi from 'app/axios/api/unitApi';
import { unitType } from '../AdmUnitPage/slice/unitType';
import { showNoti } from 'app/components/Notification/slice/notificationSlice';
const { Option } = Select;

const tailLayout = {
  wrapperCol: { offset: 19, span: 16 },
};

const CreateSupply: React.FC = () => {
  const history = useHistory();
  const dispatch = useAppDispatch();
  const [form] = Form.useForm();
  const { listUnit, total } = useAppSelector((state) => state.unit);
  const [unit, setUnit] = useState<Array<unitType>>([]);

  useEffect(() => {
    dispatch(getListSupply({}));
  }, []);
  useEffect(() => {
    dispatch(getListSupplier());
  }, []);
  useEffect(() => {
    unitApi.getAllUnitDisabledPagination().then((res) => {
      setUnit(res.data);
    });
  }, []);
  const onFinish = async (values: any) => {
    let valueData = {
      name: values.name,
      type: values.type,
      unit: values.unit,
      describe: values.describe,
      dangerLevel: values.dangerLevel,
    };
    await supplyApi
      .createSupply(valueData)
      .then((res) => {
        dispatch(addSupply(res.data));
        dispatch(
          showNoti({
            type: 'success-small',
            title: 'Success',
            desc: 'Create new supply success',
            duration: 2000,
          }),
        );
        form.resetFields();
      })
      .catch((err) => {
        console.log(err);

        dispatch(
          showNoti({
            type: 'error-small',
            title: 'Error',
            desc: 'Something when wrong, try again',
            duration: 2000,
          }),
        );
      });
  };

  const { TextArea } = Input;

  return (
    <div className="wapper-create-supply">
      <h1 className="form-supply-title">NHẬP THÔNG TIN VẬT TƯ</h1>

      <Form layout="vertical" form={form} name="control-hooks" onFinish={onFinish}>
        <Form.Item name="name" label="Tên vật tư" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name="type" label="Nhóm vật tư" rules={[{ required: true }]}>
          <Select placeholder="Select a option and change input text above" allowClear>
            {typeOfSupply.map((type, index) => (
              <Option key={index} value={type.value}>
                {type.label}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Row justify="space-between">
          <Col span={11}>
            <Form.Item name="unit" label="Đơn vị" rules={[{ required: true }]}>
              <Select placeholder="Select a option and change input text above" allowClear>
                {unit.map((unit, index) => (
                  <Option key={index} value={unit.name}>
                    {unit.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={11}>
            <Form.Item name="dangerLevel" label="Mức đồ rủi ro" rules={[{ required: true }]}>
              <Select placeholder="Select a option and change input text above" allowClear>
                {dangerLever.map((unit, index) => (
                  <Option key={index} value={unit}>
                    {unit}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Form.Item name="describe" label="Note" rules={[{ required: false }]}>
          <TextArea rows={4} />
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Create
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default CreateSupply;
