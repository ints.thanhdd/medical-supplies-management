import { createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import supplyApi from 'app/axios/api/supplyApi';
import { InitialState, TResponseGetList } from './supplyType';

const initialState: InitialState = {
  loading: false,
  listSupply: [],
  total: 0,
};
export const getListSupply = createAsyncThunk(
  '/supply/getListSupply',
  async (query: any, thunkAPI) => {
    const response = await supplyApi.getAllSupply(query);
    return response.data;
  },
);
export const getSearchSupply = createAsyncThunk(
  '/supply/getSearchSupply',
  async (param: any, thunkAPI) => {
    const response = await supplyApi.searchSupply(param.name);
    return response.data;
  },
);

const slice = createSlice({
  name: 'supply',
  initialState,
  reducers: {
    deleteSupply: (state, action) => {
      const res = state.listSupply?.filter((re) => {
        return re.id !== action.payload.id;
      });
      state.listSupply = res;
    },
    addSupply: (state, action) => {
      state.listSupply?.push(action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getListSupply.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getListSupply.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getListSupply.fulfilled, (state, action: PayloadAction<TResponseGetList>) => {
      state.listSupply = action.payload.data;
      state.total = action.payload.total;
      state.loading = false;
    });
    builder.addCase(getSearchSupply.fulfilled, (state, action: PayloadAction<any>) => {
      state.listSupply = action.payload;
    });
  },
});

export const { actions, reducer: supplyReducer } = slice;
export const { deleteSupply, addSupply } = actions;
export default supplyReducer;
