export type SupplierType = {
  name?: string;
  type?: string;
  unit?: string;
  dangerLevel: number;
  quantity: number;
  id?: any;
};
export type InitialState = {
  loading: boolean;
  listSupply?: Array<SupplierType>;
  total?: number;
};

export type TResponseGetList = {
  data?: Array<SupplierType>;
  total?: number;
};
