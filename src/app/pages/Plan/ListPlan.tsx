import React, { useEffect, useState } from 'react';
import { Table, Divider, Popconfirm } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { Link } from 'react-router-dom';
import { getListMonthlyPlan } from '../DepartmentDetail/Plan/slice/planSlice';
import ModalPlanDetail from './component/ModalPlanDetail';
import { PlanDatatype } from '../DepartmentDetail/Plan/slice/planType';

export interface DataType {
  department?: string;
  time?: string;
  id?: any;
  key?: string;
  //   planData: Array<PlanDatatype>;
}

const ListPlan = () => {
  const dispatch = useAppDispatch();
  const [openModalPlanDetail, setOpenModalPlanDetail] = useState(false);
  const [infoPlanDtail, setInfoPlanDetail] = useState<Array<PlanDatatype>>([]);

  const { listMonthlyPlan } = useAppSelector((state) => state.plan);
  useEffect(() => {
    dispatch(getListMonthlyPlan());
  }, []);
  const columns: ColumnsType<DataType> = [
    {
      title: 'Department',
      dataIndex: 'department',
      //   colSpan: 20,
    },
    {
      title: 'Time',
      dataIndex: 'time',
    },
    {
      title: 'action',
      dataIndex: 'action',
      // render: (_, id) =>
      //   data.length >= 1 ? (
      //     <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(id)}>
      //       <a>Detail</a>
      //     </Popconfirm>
      //   ) : null,
    },
  ];
  // let dataSupply = listPlan.map((list, index) => {
  //   let time = list.createdAt.slice(0, 10);
  //   return {
  //     id: list.id,
  //     key: String(index),
  //     department: list.department.name,
  //     time: time,
  //     planData: list.planData,
  //   };
  // });

  // const data: DataType[] = dataSupply;
  const handleDelete = (id) => {
    setInfoPlanDetail(id.planData);
    setOpenModalPlanDetail(true);
  };

  return (
    <div className="wapper-listsuply">
      {openModalPlanDetail && (
        <ModalPlanDetail
          isOpen={openModalPlanDetail}
          onClose={() => {
            setOpenModalPlanDetail(false);
          }}
          planDetail={infoPlanDtail}
        />
      )}
      <Divider style={{ fontSize: '22px', fontWeight: '600' }}>DANH CÁC DỰ TRÙ</Divider>
      {/* <Table columns={columns} dataSource={data} size="middle" /> */}
    </div>
  );
};
export default ListPlan;
