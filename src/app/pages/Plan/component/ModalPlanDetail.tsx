import Modal from 'app/components/Modal/Modal';
import React from 'react';
import { Table, Divider, Popconfirm } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import '../../SupplierPage/component/ModalSupplierDetail';
import { useAppDispatch } from 'store/hooks';
import { PlanDatatype } from 'app/pages/DepartmentDetail/Plan/slice/planType';

interface DataType {
  name?: string;
  unit?: string;
  quantity?: number;
  _id?: any;
}
type ModalPlan = {
  isOpen: boolean;
  onClose: () => void;
  planDetail: Array<PlanDatatype>;
};
const ModalPlanDetail = ({ isOpen, onClose, planDetail }: ModalPlan) => {
  const dispatch = useAppDispatch();

  const columns: ColumnsType<DataType> = [
    {
      title: 'Name',
      dataIndex: 'name',
      // colSpan: 20,
    },

    {
      title: 'unit',
      dataIndex: 'unit',
    },
    {
      title: 'quantity',
      dataIndex: 'quantity',
    },
    // {
    //   title: 'action',
    //   dataIndex: 'action',
    //   render: (_, id) =>
    //     data.length >= 1 ? (
    //       <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(id)}>
    //         <a>Delete</a>
    //       </Popconfirm>
    //     ) : null,
    // },
  ];

  // let dataPlan = planDetail.planData;

  const handleDelete = (id) => {
    // const newData = dataPlan.filter((item) => item.key !== id.key && {});
    // // setListPlan(newData);
  };
  const data: DataType[] = planDetail;

  return (
    <Modal open={isOpen} onClose={onClose}>
      <div className="wapper-modal-add-supplier">
        <Divider>Thông tin chi tiết dự trù</Divider>
        <br></br>
        <Table columns={columns} dataSource={data} size="middle" />
      </div>
    </Modal>
  );
};
export default ModalPlanDetail;
