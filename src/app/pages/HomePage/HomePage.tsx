import { Col, Divider, Row } from 'antd';
import * as React from 'react';
import { Pie } from '@ant-design/plots';

import './HomePage.scss';
import { useEffect, useState } from 'react';
import dashboardApi from 'app/axios/api/dashboardApi';

export interface IHomePageProps {}
type DashboardType = {
  1?: number;
  2?: number;
  3?: number;
  4?: number;
};
type DashboardOverview = {
  totalDepartment?: number;
  totalSupplier?: number;
  totalSupply?: number;
};

export default function HomePage(props: IHomePageProps) {
  const [dashboardType, setDashboardType] = useState<DashboardType>({});
  const [dashboardOverview, setDashboardOverview] = useState<DashboardOverview>({});

  useEffect(() => {
    dashboardApi.getDashboardType().then((res) => {
      setDashboardType(res.data);
    });
  }, []);
  useEffect(() => {
    dashboardApi.getDashboardOverview().then((res) => {
      setDashboardOverview(res.data);
    });
  }, []);
  const data = [
    {
      type: 'Vật tư tiêu hao',
      value: dashboardType[1],
    },
    {
      type: 'Dụng cụ y tế',
      value: dashboardType[2],
    },
    {
      type: 'Hóa chất',
      value: dashboardType[3],
    },
    {
      type: 'Sinh phẩm xét nghiệm',
      value: dashboardType[4],
    },
  ];
  const config = {
    appendPadding: 10,
    data,
    angleField: 'value',
    colorField: 'type',
    radius: 0.8,
    label: {
      type: 'outer',
      content: '{name} {percentage}',
    },
    interactions: [
      {
        type: 'pie-legend-active',
      },
      {
        type: 'element-active',
      },
    ],
  };

  return (
    <>
      <Divider style={{ fontSize: '24px', fontWeight: '600', marginTop: '0' }} orientation="center">
        Bảng điều khiển
      </Divider>
      <Row>
        <Col style={{ fontSize: '18px', marginBottom: '20px', fontWeight: '600' }} span={24}>
          Thống kê
        </Col>
        <Col style={{ marginBottom: '60px' }} span={24}>
          <Row justify="space-between">
            <Col style={{ display: 'flex', justifyContent: 'center' }} span={7}>
              <div className="item-statistical">
                <div className="item-statistical-icon">
                  <i
                    style={{ fontSize: '40px', color: 'rgb(0,158,251)' }}
                    className="fa-solid fa-suitcase-medical"
                  ></i>
                </div>
                <p>Số vật tư: {dashboardOverview.totalSupply}</p>
              </div>
            </Col>
            <Col style={{ display: 'flex', justifyContent: 'center' }} span={7}>
              <div className="item-statistical">
                <div className="item-statistical-icon">
                  <i
                    style={{ fontSize: '32px', color: 'rgb(0,158,251)' }}
                    className="fa-solid fa-city"
                  ></i>
                </div>
                <p>Số nhà cung cấp: {dashboardOverview.totalSupplier}</p>
              </div>
            </Col>
            <Col style={{ display: 'flex', justifyContent: 'center' }} span={7}>
              <div className="item-statistical">
                <div className="item-statistical-icon">
                  <i
                    style={{ fontSize: '32px', color: 'rgb(0,158,251)' }}
                    className="fa-solid fa-house"
                  ></i>
                </div>
                <p>Số khoa phòng: {dashboardOverview.totalDepartment}</p>
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <Row justify="center">
            <Col style={{ fontSize: '18px', fontWeight: '600' }} span={24}>
              Thống kê theo loại vật tư
            </Col>
            <Col span={20}>
              <Pie height={300} {...config} />
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
}
