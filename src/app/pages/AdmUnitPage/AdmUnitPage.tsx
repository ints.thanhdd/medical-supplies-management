import React, { useEffect, useState } from 'react';
import { Table, Divider, Popconfirm, Pagination, Row, Select } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import supplyApi from 'app/axios/api/supplyApi';
import { Link, useLocation, useHistory } from 'react-router-dom';
import { mPath } from 'app/constant/route';
import { createQueryUrl, parseSearchParams } from 'app/helper/queryUrl';
// import './supplyPage.scss';
import { useTranslation } from 'react-i18next';
import { translations } from 'locales/translations';
import { deleteSupply } from '../SupplyPage/slice/supplySlice';
import { deleteUnit, getUnit } from './slice/unitSlice';
import './AdmUnitPage.scss';
import ModalAddUnit from './component/ModalAddUnit';
import unitApi from 'app/axios/api/unitApi';
import { showNoti } from 'app/components/Notification/slice/notificationSlice';
interface DataType {
  name?: string;
  id?: any;
  key?: string;
}

const AdmUnitPage: React.FC = () => {
  const dispatch = useAppDispatch();
  const location = useLocation();
  const history = useHistory();
  const { t } = useTranslation();
  const { sort } = parseSearchParams(location.search);
  const { listUnit, total } = useAppSelector((state) => state.unit);
  const [openModalAddUnit, setOpenModalAddUnit] = useState(false);

  const [curPage, setCurPage] = useState(1);
  useEffect(() => {
    dispatch(getUnit({ page: curPage, sort }));
  }, [curPage, sort, dispatch]);

  const columns: ColumnsType<DataType> = [
    {
      title: 'Id',
      dataIndex: 'id',
      width: '10%',
    },
    {
      title: 'Đơn vị',
      dataIndex: 'name',
      width: '75%',
    },

    {
      title: '',
      dataIndex: 'action',
      width: '15%',
      render: (_, data) =>
        dataSupply && dataSupply?.length >= 1 ? (
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(data.id)}>
            <a href=" ">Delete</a>
          </Popconfirm>
        ) : null,
    },
  ];

  let dataSupply = listUnit?.map((list, index) => {
    return { ...list, key: String(index) };
  });
  const handleDelete = async (id) => {
    await unitApi
      .deleteUnit(id)
      .then(() => {
        dispatch(deleteUnit(id));
        dispatch(
          showNoti({
            type: 'success-small',
            title: ' Delete success',
            duration: 2000,
          }),
        );
      })
      .catch(() =>
        dispatch(
          showNoti({
            type: 'error-small',
            title: 'Something error',
            duration: 2000,
          }),
        ),
      );
  };
  const onChangePage = (e: number) => {
    setCurPage(e);
    history.replace(createQueryUrl(location, { page: e }));
  };
  const onSort = (data: string) => {
    history.replace(createQueryUrl(location, { sort: data === 'new' ? 'desc' : 'asc' }));
  };
  return (
    <div className="wapper-listsuply">
      <ModalAddUnit
        isOpen={openModalAddUnit}
        onClose={() => {
          setOpenModalAddUnit(false);
        }}
      />
      <Divider style={{ fontSize: '24px', fontWeight: '600' }}>
        Danh sách đơn vị
        {/* {t(translations.SUPPLY_LIST.LIST_SUPPLY)} */}
      </Divider>
      <Row justify="space-between">
        <div className="sort-container">
          <div className="sort-wrapper">
            <div
              className={`sort-item ${sort === 'desc' ? 'active' : ''}`}
              onClick={() => onSort('new')}
            >
              <i className="fa-solid fa-arrow-up-wide-short"></i>
              <p>Lastest</p>
            </div>
            <div
              className={`sort-item ${sort === 'asc' ? 'active' : ''}`}
              onClick={() => onSort('old')}
            >
              <i className="fa-solid fa-arrow-down-wide-short"></i>
              <p>Oldest</p>
            </div>
          </div>
        </div>

        <div
          className="clear-filter"
          onClick={() => {
            setOpenModalAddUnit(true);
          }}
        >
          <button className="staff-btn-add">Thêm mới</button>
        </div>
      </Row>
      <div className="table-supply">
        <Table columns={columns} dataSource={dataSupply} size="middle" pagination={false} />
      </div>
      <Row justify="center">
        <Pagination defaultCurrent={1} current={curPage} total={total} onChange={onChangePage} />
      </Row>
    </div>
  );
};

export default AdmUnitPage;
