import { Button, Form, Input, Select } from 'antd';
import departmentStaffApi from 'app/axios/api/departmentStaffApi';
import unitApi from 'app/axios/api/unitApi';
import Modal from 'app/components/Modal/Modal';
import React from 'react';
import { useAppDispatch } from 'store/hooks';
import { addUnit } from '../slice/unitSlice';
type Props = {
  isOpen: boolean;
  onClose: () => void;
};
const ModalAddUnit = ({ isOpen, onClose }: Props) => {
  const dispatch = useAppDispatch();
  const onFinish = (values: any) => {
    unitApi.createUnit(values).then((res) => {
      dispatch(addUnit(res.data));
    });
    onClose();
  };

  return (
    <Modal open={isOpen} onClose={onClose}>
      <div className="department-wapper">
        <h1 className="department-title">Thêm mới đơn vị</h1>
        <div className="department-form-edit">
          <Form
            name="basic"
            labelCol={{ span: 0 }}
            wrapperCol={{ span: 24 }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              name="name"
              rules={[{ min: 1, required: true, message: 'Please input unit name!' }]}
            >
              <Input placeholder="Name" />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 10, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </Modal>
  );
};
export default ModalAddUnit;
