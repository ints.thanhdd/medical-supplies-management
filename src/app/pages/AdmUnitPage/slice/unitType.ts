export type unitType = {
    name?: string;
    id?: any;
  };
  export type InitialState = {
    loading: boolean;
    listUnit?: Array<unitType>;
    total?: number;
  };
  
  export type TResponseGetList = {
    data?: Array<unitType>;
    total?: number;
  };
  