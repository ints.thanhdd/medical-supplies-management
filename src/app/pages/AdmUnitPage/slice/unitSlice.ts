import { createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import supplyApi from 'app/axios/api/supplyApi';
import unitApi from 'app/axios/api/unitApi';
import { InitialState, TResponseGetList } from './unitType';

const initialState: InitialState = {
  loading: false,
  listUnit: [],
  total: 0,
};
export const getUnit = createAsyncThunk('/unit/getListSupply', async (query: any, thunkAPI) => {
  const response = await unitApi.getAllUnit(query);
  return response.data;
});
export const getSearchSupply = createAsyncThunk(
  '/supply/getSearchSupply',
  async (param: any, thunkAPI) => {
    const response = await supplyApi.searchSupply(param.name);
    return response.data;
  },
);

const slice = createSlice({
  name: 'supply',
  initialState,
  reducers: {
    deleteUnit: (state, action) => {
      console.log(action.payload);

      const listUnitRemain = state.listUnit?.filter((u) => {
        return u.id !== action.payload;
      });
      state.listUnit = listUnitRemain;
    },
    addUnit: (state, action) => {
      state.listUnit?.push(action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getUnit.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getUnit.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getUnit.fulfilled, (state, action: PayloadAction<TResponseGetList>) => {
      state.listUnit = action.payload.data;
      state.total = action.payload.total;
    });
    builder.addCase(getSearchSupply.fulfilled, (state, action: PayloadAction<any>) => {
      state.listUnit = action.payload;
      console.log(action.payload);
    });
  },
});

export const { actions, reducer: unitReducer } = slice;
export const { deleteUnit, addUnit } = actions;
export default unitReducer;
