import { Button, Form, Input, Select } from 'antd';
import departmentStaffApi from 'app/axios/api/departmentStaffApi';
import Modal from 'app/components/Modal/Modal';
import { showNoti } from 'app/components/Notification/slice/notificationSlice';
import { position } from 'app/constant/position';
import React from 'react';
import { useAppDispatch } from 'store/hooks';
import { addDepartmentStaff } from '../slice/departmentStaffSlice';
type Props = {
  isOpen: boolean;
  onClose: () => void;
};
const { Option } = Select;

const ModalAddDepartmentStaff = ({ isOpen, onClose }: Props) => {
  const dispatch = useAppDispatch();
  const user = JSON.parse(localStorage.getItem('dataLogin') ?? '');

  const onFinish = async (values: any) => {
    await departmentStaffApi
      .createDepartmentStaff(user.department, values)
      .then((res) => {
        dispatch(addDepartmentStaff(res.data));
        dispatch(
          showNoti({
            type: 'success-small',
            title: 'Tạo mới nhân viên thành công',
            duration: 2000,
          }),
        );
        onClose();
      })
      .catch(() => {
        dispatch(
          showNoti({
            type: 'error-small',
            title: 'Something error',
            duration: 2000,
          }),
        );
      });
  };

  return (
    <Modal open={isOpen} onClose={onClose}>
      <div className="department-wapper">
        <h1 className="department-title">Thêm mới nhân viên</h1>
        <div className="department-form-edit">
          <Form
            name="basic"
            labelCol={{ span: 0 }}
            wrapperCol={{ span: 24 }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              name="name"
              rules={[{ min: 3, required: true, message: 'Please input departmentname!' }]}
            >
              <Input placeholder="Name" />
            </Form.Item>

            <Form.Item
              name="username"
              rules={[{ required: true, message: 'Please input your username' }]}
            >
              <Input placeholder="Username" />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[{ required: true, message: 'Please input your password!' }]}
            >
              <Input placeholder="password" />
            </Form.Item>
            <Form.Item
              name="email"
              rules={[{ required: true, message: 'Please input your email!' }]}
            >
              <Input placeholder="Email" />
            </Form.Item>
            <Form.Item name="position" label="Chức vụ" rules={[{ required: true }]}>
              <Select placeholder="Chọn chức vụ" allowClear>
                {position.map((pst, index) => (
                  <Option
                    // onClick={() => {
                    //   setUnitSupply(supply.unit!);
                    // }}
                    value={pst}
                    key={index}
                  >
                    {pst}
                  </Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 10, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </Modal>
  );
};
export default ModalAddDepartmentStaff;
