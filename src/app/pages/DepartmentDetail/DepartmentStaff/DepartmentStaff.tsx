import React, { useEffect, useState } from 'react';
import { Button, Divider, Popconfirm, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import './DepartmentStaff.scss';
import ModalAddDepartmentStaff from './component/ModalAddDepartmentStaff';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { deleteDepartmentStaff, getListDepartmentStaff } from './slice/departmentStaffSlice';
import departmentStaffApi from 'app/axios/api/departmentStaffApi';

interface DataType {
  // key: React.Key;
  id?: string;
  name?: string;
  address?: string;
  position?: string;
  username?: string;
}

const DepartmentStaff: React.FC = () => {
  const dispatch = useAppDispatch();
  const [openModalAddDepartmentStaff, setOpenModalAddDepartmentStaff] = useState(false);
  const { listDepartmentStaff } = useAppSelector((state) => state.departmentStaff);
  const user = JSON.parse(localStorage.getItem('dataLogin') ?? '');
  useEffect(() => {
    dispatch(getListDepartmentStaff({ id: user.department }));
  }, []);

  const columns: ColumnsType<DataType> = [
    { title: 'Id', dataIndex: 'id', key: 'id' },
    { title: 'Name', dataIndex: 'name', key: 'name' },
    { title: 'Position', dataIndex: 'position', key: 'position' },
    { title: 'Email', dataIndex: 'email', key: 'email' },
    { title: 'UserName', dataIndex: 'username', key: 'username' },

    {
      title: 'Action',
      dataIndex: '',
      key: 'x',
      render: (_, record) =>
        data.length >= 1 ? (
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.id)}>
            <a href=" ">Delete</a>
          </Popconfirm>
        ) : null,
    },
  ];
  const handleDelete = async (id) => {
    await departmentStaffApi.deleteDepartmentStaff(id).then((res) => {
      dispatch(deleteDepartmentStaff(id));
    });
  };
  let dataStaff = listDepartmentStaff
    .filter((s) => s.role === 'staff')
    .map((list, index) => {
      return { ...list, key: String(index) };
    });
  const data: DataType[] = dataStaff;
  return (
    <div className="department-staff">
      <ModalAddDepartmentStaff
        isOpen={openModalAddDepartmentStaff}
        onClose={() => {
          setOpenModalAddDepartmentStaff(false);
        }}
      />
      <Divider>Danh sách nhân viên</Divider>
      <Button
        onClick={() => {
          setOpenModalAddDepartmentStaff(true);
        }}
        className="staff-btn-add"
      >
        Thêm mới
      </Button>
      <br></br>
      <br></br>
      <Table columns={columns} dataSource={dataStaff} />
    </div>
  );
};

export default DepartmentStaff;
