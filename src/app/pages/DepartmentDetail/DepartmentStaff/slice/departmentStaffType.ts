export type DepartmentStaffType = {
  name?: string;
  username?: string;
  address?: string;
  email?: string;
  position?: string;
  id?: any;
  role?: string;
};
export type InitialState = {
  loading: boolean;
  listDepartmentStaff: Array<DepartmentStaffType>;
};
