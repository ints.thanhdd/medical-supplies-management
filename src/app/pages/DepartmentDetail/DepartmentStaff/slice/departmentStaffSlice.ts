import { createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import departmentStaffApi from 'app/axios/api/departmentStaffApi';
import { InitialState } from './departmentStaffType';

const initialState: InitialState = {
  loading: false,
  listDepartmentStaff: [],
};
export const getListDepartmentStaff = createAsyncThunk(
  '/departmentStaff/getListDepartmentStaff',
  async (param: any, thunkAPI) => {
    const response = await departmentStaffApi.getAllDepartmentStaff(param.id);
    return response.data;
  },
);

const slice = createSlice({
  name: 'departmentStaff',
  initialState,
  reducers: {
    deleteDepartmentStaff: (state, action) => {
      const listStaffRemain = [...state.listDepartmentStaff].filter((s) => s.id !== action.payload);
      state.listDepartmentStaff = listStaffRemain;
    },
    addDepartmentStaff: (state, action) => {
      state.listDepartmentStaff.push(action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getListDepartmentStaff.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getListDepartmentStaff.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(
      getListDepartmentStaff.fulfilled,
      (state, action: PayloadAction<Array<any>>) => {
        state.listDepartmentStaff = action.payload;
      },
    );
  },
});

export const { actions, reducer: departmentStaffReducer } = slice;
export const { deleteDepartmentStaff, addDepartmentStaff } = actions;
export default departmentStaffReducer;
