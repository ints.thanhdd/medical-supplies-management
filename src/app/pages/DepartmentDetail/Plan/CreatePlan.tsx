import React, { useEffect, useState } from 'react';
import { Table, Divider, Popconfirm, Row, Col } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { getListSupply } from 'app/pages/SupplyPage/slice/supplySlice';
import { Button, Form, Input, Select } from 'antd';
import './CreatePlan.scss';
import { SendOutlined } from '@ant-design/icons';
import planApi from 'app/axios/api/planApi';
import { showNoti } from 'app/components/Notification/slice/notificationSlice';
import { addPlan } from './slice/planSlice';
import { getUnit } from 'app/pages/AdmUnitPage/slice/unitSlice';
import { typeOfSupply } from 'app/constant/common';

interface DataType {
  name: string;
  unit?: string;
  quantity?: number;
  model?: string;
  serial?: string;
  origin?: string;
  type?: string;
  id?: any;
  key?: string;
}
const { Option } = Select;

const tailLayout = {
  wrapperCol: { offset: 0, span: 16 },
};

const ListSupply: React.FC = () => {
  const dispatch = useAppDispatch();

  const [listPlan, setListPlan] = useState<Array<DataType>>([]);
  const user = JSON.parse(localStorage.getItem('dataLogin') ?? '');
  const { listSupply } = useAppSelector((state) => state.supply);
  console.log(user);
  useEffect(() => {
    dispatch(getListSupply({}));
  }, []);
  useEffect(() => {
    dispatch(getUnit({}));
  }, []);
  const [form] = Form.useForm();
  const { TextArea } = Input;
  const onFinish = (values: any) => {
    let value = { ...values, quantity: +values.quantity };

    let list = dataPlan.filter((list) => {
      return list.name === values.name;
    });
    if (list.length > 0) {
      setListPlan(listPlan);
      dispatch(
        showNoti({
          type: 'error',
          title: 'materials already available',
          duration: 3000,
        }),
      );
    } else {
      setListPlan((pre) => {
        return [...pre, value];
      });
    }
  };
  const columns: ColumnsType<DataType> = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Type',
      dataIndex: 'type',
    },
    {
      title: 'Model',
      dataIndex: 'model',
    },
    {
      title: 'Serial',
      dataIndex: 'serial',
    },
    {
      title: 'Unit',
      dataIndex: 'unit',
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
    },

    {
      title: 'Action',
      dataIndex: 'action',
      render: (_, id) =>
        data.length >= 1 ? (
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(id)}>
            <a>Delete</a>
          </Popconfirm>
        ) : null,
    },
  ];
  let dataPlan = listPlan.map((list, index) => {
    return { ...list, key: String(index) };
  });
  const handleDelete = (id) => {
    const newData = dataPlan.filter((item) => item.key !== id.key && {});
    setListPlan(newData);
  };
  const data: DataType[] = dataPlan;
  const handleSendPlan = async () => {
    const sendData = {
      ownerName: user.name,
      planType: 3,
      department: user.department,
      planData: data,
      note: form.getFieldValue('note'),
    };
    await planApi.createPlan(sendData).then((res) => {
      dispatch(addPlan(res.data));
    });
    dispatch(
      showNoti({
        type: 'success-small',
        title: 'Send monthly plan successfully',
        duration: 3000,
      }),
    );
  };
  const handleChange = (value: string) => {
    listSupply?.filter((e) => {
      if (e.name === value) {
        form.setFieldValue('unit', e.unit);
      }
    });
  };
  return (
    <div className="wapper-listsuply">
      <Divider style={{ marginBottom: '10px' }}>LẬP DỰ TRÙ</Divider>
      <Form size="small" layout="vertical" form={form} name="control-hooks" onFinish={onFinish}>
        <Row gutter={[16, 0]}>
          <Col span={8}>
            <Form.Item name="name" label="Tên vật tư" rules={[{ required: true }]}>
              <Select onChange={handleChange} allowClear>
                {listSupply?.map((supply, index) => (
                  <Option value={supply.name} key={index}>
                    {supply.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item name="type" label="Loại vật tư" rules={[{ required: true }]}>
              <Select placeholder="Select a option and change input text above" allowClear>
                {typeOfSupply.map((type, index) => (
                  <Option value={type.label} key={index}>
                    {type.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item name="model" label="Model" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item name="serial" label="Serial" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="unit" label="Đơn vị" rules={[{ required: true }]}>
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item name="origin" label="Xuất xứ" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
          </Col>

          <Col span={4}>
            <Form.Item name="quantity" label="Số lượng" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item name="note" label="Note" rules={[{ required: false }]}>
              <TextArea rows={2} />
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item {...tailLayout}>
              <Button className="create-plan-btn" type="primary" htmlType="submit">
                Add
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
      {/* ) : null} */}

      <Table columns={columns} dataSource={data} size="middle" />
      <div onClick={handleSendPlan} className="plan-send">
        <span>Send plan</span>
        <SendOutlined />
      </div>
    </div>
  );
};

export default ListSupply;
