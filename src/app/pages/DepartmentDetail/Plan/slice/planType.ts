export type PlanType = {
  planData?: Array<PlanDatatype>;
  department?: { id: string; name: string };
  id?: string;
  createdAt?: string;
  note?: string;
};
export type InitialState = {
  loading: boolean;
  listMonthlyPlan: Array<PlanType>;
};
export type PlanDatatype = {
  name?: string;
  quantity?: number;
  unit?: string;
  id?: string;
  type?: string;
  serial?: string;
  model?: string;
  origin?: string;
  _id?: string;
};
