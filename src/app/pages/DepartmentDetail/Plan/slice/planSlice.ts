import { createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import planApi from 'app/axios/api/planApi';
import { InitialState } from './planType';

const initialState: InitialState = {
  loading: false,
  listMonthlyPlan: [],
};
export const getListMonthlyPlan = createAsyncThunk(
  '/plan/getListMonthlyPlan',
  async (_, thunkAPI) => {
    const response = await planApi.getMonthlyPlan();
    return response.data;
  },
);

const slice = createSlice({
  name: 'plan',
  initialState,
  reducers: {
    addPlan: (state, action) => {
      state.listMonthlyPlan.push(action.payload);
    },
    deletePlan: (state, action) => {
      const res = state.listMonthlyPlan.filter((re) => {
        return re.id !== action.payload.id;
      });
      state.listMonthlyPlan = res;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getListMonthlyPlan.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getListMonthlyPlan.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getListMonthlyPlan.fulfilled, (state, action: PayloadAction<Array<any>>) => {
      state.listMonthlyPlan = action.payload;
    });
  },
});

export const { actions, reducer: planReducer } = slice;
export const { addPlan, deletePlan } = actions;
export default planReducer;
