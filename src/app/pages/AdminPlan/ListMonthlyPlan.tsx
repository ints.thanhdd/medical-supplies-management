import React, { useEffect, useRef, useState } from 'react';
import { Divider, Row, Col, Popover } from 'antd';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { getListMonthlyPlan } from '../DepartmentDetail/Plan/slice/planSlice';
import ModalPlanDetail from './component/ModalPlanDetail';
import { PlanDatatype, PlanType } from '../DepartmentDetail/Plan/slice/planType';
import './ListMonthlyPlan.scss';
import moment from 'moment';
import { PlusSquareOutlined, MinusSquareOutlined } from '@ant-design/icons';
import TotalMonthlyPlan from './component/TotalMonthlyPlan';
export type DataType = {
  department?: string;
  time?: string;
  id?: any;
  key?: string;
};
export type TotalPlan = Array<{
  id: string | number;
  plan: Array<PlanDatatype>;
}>;
const ListMonthlyPlan = () => {
  const dispatch = useAppDispatch();
  const bubleRef = useRef<HTMLSpanElement>(null);
  const [openModalPlanDetail, setOpenModalPlanDetail] = useState(false);
  const [infoPlanDtail, setInfoPlanDetail] = useState<PlanType>({});
  const [totalPlan, setTotalPlan] = useState<TotalPlan>([]);
  const { listMonthlyPlan } = useAppSelector((state) => state.plan);
  useEffect(() => {
    dispatch(getListMonthlyPlan());
  }, []);
  const scaleAnimate = [
    {
      transform: 'rotate(0deg) scale(1) skew(1deg)',
    },
    {
      transform: 'rotate(-25deg) scale(1.05) skew(1deg)',
    },
    {
      transform: 'rotate(25deg) scale(1.1) skew(1deg)',
    },
    {
      transform: 'rotate(-25deg) scale(1.2) skew(1deg)',
    },
    {
      transform: 'rotate(25deg) scale(1.15) skew(1deg)',
    },
    {
      transform: 'rotate(0deg) scale(1.1) skew(1deg)',
    },
    {
      transform: 'rotate(0deg) scale(1) skew(1deg)',
    },
  ];

  const handleAddPlan = (plan) => {
    bubleRef.current?.animate(scaleAnimate, {
      duration: 500,
      fill: 'forwards',
    });
    setTotalPlan((prev) => [
      ...prev,
      {
        id: plan.id,
        plan: plan.planData,
      },
    ]);
  };
  const handleRemovePlan = (plan) => {
    const remainTotalPlan = totalPlan.filter((p) => p.id !== plan.id);
    setTotalPlan(remainTotalPlan);
  };
  return (
    <div className="wapper-listsuply">
      {openModalPlanDetail && (
        <ModalPlanDetail
          isOpen={openModalPlanDetail}
          onClose={() => {
            setOpenModalPlanDetail(false);
          }}
          planDetail={infoPlanDtail}
        />
      )}
      <Divider className="title-list">DANH SÁCH CÁC DỰ TRÙ</Divider>
      <Popover
        content={<TotalMonthlyPlan planData={totalPlan} />}
        title=""
        placement="bottomLeft"
        trigger="click"
      >
        <div className="total-plan">
          <p className="total-plan-btn">Tổng hợp dự trù</p>
          <span ref={bubleRef}>{totalPlan.length}</span>
        </div>
      </Popover>
      <br></br>
      <span className="btn-remove-all" onClick={() => setTotalPlan([])}>
        Clear all
        <i className="fa-solid fa-trash"></i>
      </span>
      <Row>
        {listMonthlyPlan.map((plan) => {
          return (
            <Col key={plan.id} span={24} style={{ marginBottom: '15px' }}>
              <Row className="item-listplan">
                <Col
                  onClick={() => {
                    setInfoPlanDetail(plan);
                    setOpenModalPlanDetail(true);
                  }}
                  span={21}
                  className="item-listplan-sub"
                >
                  <p className="item-listplan-title">{plan.department?.name}</p>
                  <p className="item-listplan-time">
                    {moment(plan.createdAt).format('MMMM Do YYYY, h:mm:ss a')}
                  </p>
                </Col>
                <Col className="item-listplan-icon" span={3}>
                  {!totalPlan.find((p) => p.id === plan.id) ? (
                    <PlusSquareOutlined
                      className="item-listplan-icon-check"
                      onClick={() => handleAddPlan(plan)}
                    />
                  ) : (
                    <MinusSquareOutlined
                      className="item-listplan-icon-close"
                      onClick={() => handleRemovePlan(plan)}
                    />
                  )}
                </Col>
              </Row>
            </Col>
          );
        })}
      </Row>
    </div>
  );
};
export default ListMonthlyPlan;
