import React from 'react';
import zipcelx from 'zipcelx';
import Table from 'antd/lib/table';
import { TotalPlan } from '../ListMonthlyPlan';
import { PlanDatatype } from 'app/pages/DepartmentDetail/Plan/slice/planType';
import { useDispatch } from 'react-redux';
import { showNoti } from 'app/components/Notification/slice/notificationSlice';
type Props = {
  planData: TotalPlan;
};

const TotalMonthlyPlan = ({ planData }: Props) => {
  const dispatch = useDispatch();
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Type',
      dataIndex: 'type',
    },
    {
      title: 'Model',
      dataIndex: 'model',
    },
    {
      title: 'Serial',
      dataIndex: 'serial',
    },
    {
      title: 'Unit',
      dataIndex: 'unit',
    },
    {
      title: 'Origin',
      dataIndex: 'origin',
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
    },
  ];
  const calculatedPlan = planData
    .reduce((acc: Array<PlanDatatype>, cur) => [...acc].concat(cur.plan), [])
    .reduce((results: Array<PlanDatatype>, sup: PlanDatatype) => {
      if (
        results.find(
          (s) =>
            s.name === sup.name &&
            s.model === sup.model &&
            s.type === sup.type &&
            s.serial === sup.serial &&
            s.origin === sup.origin &&
            s.unit === sup.unit,
        )
      ) {
        const updateQuantity = results.map((e) =>
          e.name === sup.name &&
          e.type === sup.type &&
          e.model === sup.model &&
          e.origin === sup.origin &&
          e.serial === sup.serial
            ? { ...e, quantity: (e.quantity ?? 0) + (sup.quantity ?? 0) }
            : e,
        );
        return updateQuantity;
      }
      return [...results, sup];
    }, []);
  const dataTable = calculatedPlan.map((e, i) => ({ ...e, key: i.toString() }));
  const dataToExport = dataTable.map((e) => {
    const arrayObj = Object.entries(e);
    return arrayObj.map((v) =>
      v[0] !== '_id' && v[0] !== 'key' ? { value: v[1].toString(), type: 'string' } : {},
    );
  });
  const config = {
    filename: 'total-monthly-plan',
    sheet: {
      data: [
        [
          {
            value: 'Name',
            type: 'string',
          },
          {
            value: 'Type',
            type: 'string',
          },
          {
            value: 'Model',
            type: 'string',
          },
          {
            value: 'Serial',
            type: 'string',
          },
          {
            value: 'Unit',
            type: 'string',
          },
          {
            value: 'Origin',
            type: 'string',
          },
          {
            value: 'Quantity',
            type: 'string',
          },
        ],
        ...dataToExport,
      ],
    },
  };
  const handelExportFile = () => {
    if (dataToExport.length > 0) zipcelx(config);
    else
      dispatch(
        showNoti({
          title: 'Something error !!',
          type: 'error-small',
          duration: 2000,
          desc: 'No data to export',
        }),
      );
  };
  console.log(dataToExport);
  return (
    <div className="total-plan-popover">
      <Table
        className="total-plan-table"
        columns={columns}
        dataSource={dataTable}
        size="middle"
        pagination={false}
      />
      <button className="btn-export-plan" onClick={handelExportFile}>
        Export to excel
        <i className="fa-solid fa-file-export"></i>
      </button>
    </div>
  );
};

export default TotalMonthlyPlan;
