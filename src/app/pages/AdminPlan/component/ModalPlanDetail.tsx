import Modal from 'app/components/Modal/Modal';
import React from 'react';
import { Table, Divider, Popconfirm } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import '../../SupplierPage/component/ModalSupplierDetail';
import { PlanDatatype, PlanType } from 'app/pages/DepartmentDetail/Plan/slice/planType';
import './ModalPlanDetail.scss';
import { log } from 'console';

interface DataType {
  name?: string;
  unit?: string;
  quantity?: number;
  id?: any;
}
type ModalPlan = {
  isOpen: boolean;
  onClose: () => void;
  planDetail: PlanType;
};
const ModalPlanDetail = ({ isOpen, onClose, planDetail }: ModalPlan) => {
  const columns: ColumnsType<DataType> = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Type',
      dataIndex: 'type',
    },
    {
      title: 'Model',
      dataIndex: 'model',
    },
    {
      title: 'Serial',
      dataIndex: 'serial',
    },
    {
      title: 'Unit',
      dataIndex: 'unit',
    },
    {
      title: 'Origin',
      dataIndex: 'origin',
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
    },
  ];
  const data: DataType[] = planDetail.planData?.map((e, i) => ({ ...e, key: i })) ?? [];
  console.log(planDetail.planData);
  return (
    <Modal open={isOpen} onClose={onClose}>
      <div className="wapper-modal-add-supplier">
        <Divider>Thông tin chi tiết dự trù</Divider>
        <br></br>
        <p className="plan-note">{planDetail.note ?? ''}</p>
        <Table columns={columns} dataSource={data} size="middle" />
      </div>
    </Modal>
  );
};
export default ModalPlanDetail;
