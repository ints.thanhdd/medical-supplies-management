import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  BellOutlined,
} from '@ant-design/icons';
import { Badge, Layout, Menu, Select } from 'antd';
import React, { useState } from 'react';
import './DefaultLayout.scss';
import type { MenuProps } from 'antd';
import { MedicineBoxOutlined, HomeOutlined } from '@ant-design/icons';
import { Link, useHistory } from 'react-router-dom';
import i18next from 'i18next';
import { useTranslation } from 'react-i18next';
import { translations } from 'locales/translations';
import { getLanguageFromLocalStorage, getUserFromLocalStorage } from 'app/helper/localStorage';
import vnFlag from '../../../images/vn-flag.png';
import enFlag from '../../../images/en-flag.png';
import { mPath } from 'app/constant/route';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'store/configureStore';
import { getPublicUrl } from 'app/helper/functions';
import { logoutUser } from 'app/pages/AuthPage/slice/authSlice';

const handleChangeLanguage = (value: string) => {
  localStorage.setItem('language', value);
  i18next.changeLanguage(value);
};
const { Header, Sider, Content } = Layout;

type Props = {
  children: JSX.Element;
};
const DefaultLayout: React.FC<Props> = ({ children }) => {
  const { t } = useTranslation();
  const histoty = useHistory();
  const dispatch = useDispatch();
  const languageUser = getLanguageFromLocalStorage();
  const [onSetting, setOnSetting] = useState(false);
  const userInfo = useSelector((state: RootState) => state.auth).user ?? getUserFromLocalStorage();
  const listSubnavAdmin = [
    {
      id: 1,
      icon: HomeOutlined,
      label: <Link to={mPath.A_HOME}>Home</Link>,
    },
    {
      id: 2,
      icon: MedicineBoxOutlined,
      label: t(translations.HOMEPAGE.SUPPLY),
      children: [
        {
          label: <Link to={mPath.A_SUPPLY_CREATE}>{t(translations.HOMEPAGE.ADD_SUPPLY)}</Link>,
        },
        {
          label: <Link to={mPath.A_SUPPLY_LIST}>{t(translations.HOMEPAGE.LIST_SUPPLY)}</Link>,
        },
      ],
    },
    {
      id: 3,
      icon: MedicineBoxOutlined,
      label: 'Plan',
      children: [
        {
          label: <Link to={mPath.A_PLAN_LIST}>List Plan</Link>,
        },
        {
          label: <Link to={mPath.A_WAREHOUSE}>List request supply</Link>,
        },
      ],
    },
    {
      id: 4,
      icon: HomeOutlined,
      label: <Link to={mPath.A_SUPPLIER}>{t(translations.HOMEPAGE.SUPPLIER)}</Link>,
    },
    {
      id: 5,
      icon: HomeOutlined,
      label: <Link to={mPath.A_DEPARTMENTS}>{t(translations.HOMEPAGE.DEPARTMENT)}</Link>,
    },
    {
      id: 6,
      icon: UserOutlined,
      label: <Link to={mPath.A_UNIT}>Đơn vị</Link>,
    },
    {
      id: 7,
      icon: UserOutlined,
      label: <Link to={mPath.A_WAREHOUSE}>{t(translations.HOMEPAGE.STATISTICAL)}</Link>,
    },
  ];

  const listSubnavDepartment = [
    {
      id: 1,
      icon: MedicineBoxOutlined,
      label: <Link to={mPath.M_PLAN_CREATE}>Lập dự trù</Link>,
    },
    {
      id: 2,
      icon: MedicineBoxOutlined,
      label: 'Hoàn trả vật tư',
    },
    {
      id: 3,
      icon: MedicineBoxOutlined,
      label: 'Lấy thêm vật tư',
    },
    {
      id: 4,
      icon: MedicineBoxOutlined,
      label: <Link to={mPath.M_WAREHOUSE}>Kho vật tư</Link>,
    },
    {
      id: 5,
      icon: MedicineBoxOutlined,
      label: <Link to={mPath.M_STAFF}>Nhân viên</Link>,
    },
  ];
  const itemsAdmin: MenuProps['items'] = listSubnavAdmin.map((list, index) => {
    const key = String(index + 1);

    return {
      key: `sub${key}`,
      icon: React.createElement(list.icon),
      label: list.label,
      children: list.children?.map((_, j) => {
        const subKey = index * 4 + j + 1;
        return {
          key: subKey,
          label: _.label,
        };
      }),
    };
  });
  const itemsDepartment: MenuProps['items'] = listSubnavDepartment.map((list, index) => {
    const key = String(index + 1);

    return {
      key: `sub${key}`,
      icon: React.createElement(list.icon),
      label: list.label,
    };
  });

  const [collapsed, setCollapsed] = useState(false);
  const onLogout = () => {
    histoty.push(mPath.LOGIN);
    dispatch(logoutUser({}));
  };
  return (
    <Layout className="defaultlayout-wapper">
      <Sider className="sider-wapper" trigger={null} collapsible collapsed={collapsed}>
        <div className="sider-title">
          <div className="sider-logo">
            <img className="logo-img" src={getPublicUrl('logo_app.png')} alt="" />
          </div>
        </div>
        <Menu
          theme="dark"
          mode="inline"
          style={{ borderRight: 0 }}
          items={userInfo.role === 'management' ? itemsAdmin : itemsDepartment}
        />
      </Sider>
      <Layout className="site-layout default-layout-body">
        <Header className="header site-layout-background" style={{ padding: 20 }}>
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: () => setCollapsed(!collapsed),
          })}
          <div className="logo-user">
            <Select
              defaultValue={languageUser}
              style={{ width: 120 }}
              onChange={handleChangeLanguage}
              options={[
                {
                  value: 'vn',
                  label: (
                    <>
                      <img className="language-flag" src={vnFlag} alt="" />
                      <span>Việt Nam</span>
                    </>
                  ),
                },
                {
                  value: 'en',
                  label: (
                    <>
                      <img className="language-flag" src={enFlag} alt="" />
                      <span>English</span>
                    </>
                  ),
                },
              ]}
            ></Select>
            <div className="logo-bell">
              <Badge count={99} overflowCount={10}>
                <BellOutlined className="bell" />
              </Badge>
            </div>

            <div className="user-settings" onClick={() => setOnSetting((prev) => !prev)}>
              <i className="fa-solid fa-user"></i>
            </div>
            {onSetting && (
              <ul className="user-settings-list">
                <li onClick={onLogout}>Logout</li>
              </ul>
            )}
          </div>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            margin: '10px 5px 5px 10px',
            padding: '10px 16px',
            overflow: 'scroll',
            overflowX: 'hidden',
          }}
        >
          <div className="default-layout-content">{children}</div>
        </Content>
      </Layout>
    </Layout>
  );
};

export default DefaultLayout;
