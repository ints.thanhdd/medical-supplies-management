import axios from 'axios';
import { axiosClient } from '../axiosClient';

const planDepartmentApi = {
  getMonthlyPlan: () => {
    const url = `/plan-department/monthly`;
    return axiosClient.get(url);
  },
  createPlan: (payload) => {
    const url = '/plan-department';
    return axiosClient.post(url, payload);
  },
  deletePlan: (id) => {
    const url = `/plan-department/${id}`;
    return axiosClient.delete(url);
  },
};

export default planDepartmentApi;
