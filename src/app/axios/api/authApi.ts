import { axiosClient } from '../axiosClient';

const authApi = {
  loginUser: (payload) => {
    const url = 'http://localhost:8001/v1/auth/login';
    return axiosClient.post(url, payload);
  },
  registerUser: (payload) => {
    const url = 'http://localhost:8001/v1/auth/register';
    return axiosClient.post(url, payload);
  },
  requestRefreshToken: (payload) => {
    const url = 'http://localhost:8001/v1/auth/refresh';
    return axiosClient.post(url, payload);
  },
};

export default authApi;
