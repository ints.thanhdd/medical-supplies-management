import { axiosClient } from '../axiosClient';

const dashboardApi= {
  getDashboardType: () => {
    const url = `/dashboard/type`;
    return axiosClient.get(url);
  },
  getDashboardOverview: () => {
    const url = `/dashboard/overview`;
    return axiosClient.get(url);
  },
  
};

export default dashboardApi;