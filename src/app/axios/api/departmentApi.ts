import { axiosClient } from '../axiosClient';

const departmentApi = {
  getAllDepartment: () => {
    const url = `/department`;
    return axiosClient.get(url);
  },
  getDepartmentDetail: (id) => {
    const url = `/department/${id}`;
    return axiosClient.get(url);
  },
  deleteDepartment: (id) => {
    const url = `/department/${id}`;
    return axiosClient.delete(url);
  },
  searchDepartment: (name) => {
    const url = `/department/search?q=${name}`;
    return axiosClient.get(url);
  },
  createDepartment: (payload) => {
    const url = '/department';
    return axiosClient.post(url, payload);
  },
  patchDepartment: (department) => {
    const url = '/department';
    return axiosClient.patch(url, department);
  },
};

export default departmentApi;
