import { axiosClient } from '../axiosClient';
import queryString from 'query-string';
const supplyApi = {
  getAllSupply: (query) => {
    const queryParams = queryString.stringify(query);
    const url = `/supply?${queryParams}`;
    return axiosClient.get(url);
  },
  deleteSupply: (id) => {
    const url = `/supply/${id}`;
    return axiosClient.delete(url);
  },
  searchSupply: (name) => {
    const url = `/supply/search?q=${name}`;
    return axiosClient.get(url);
  },
  createSupply: (payload) => {
    const url = '/supply';
    return axiosClient.post(url, payload);
  },
  patchSupply: (supply) => {
    const url = '/supply';
    return axiosClient.patch(url, supply);
  },
};

export default supplyApi;
