import { axiosClient } from '../axiosClient';

const departmentStaffApi = {
  getAllDepartmentStaff: (id) => {
    const url = `/user/${id}`;
    return axiosClient.get(url);
  },
  createDepartmentStaff: (id, payload) => {
    const url = `/user/${id}`;
    return axiosClient.post(url, payload);
  },
  deleteDepartmentStaff: (staffId) => {
    const url = `/user/${staffId}`;
    return axiosClient.delete(url);
  },
  patchDepartmentStaff: (id, department) => {
    const url = `/user/${id}`;
    return axiosClient.patch(url, department);
  },
};

export default departmentStaffApi;
