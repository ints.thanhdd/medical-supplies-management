import axios from 'axios';
import { axiosClient } from '../axiosClient';

const supplierApi = {
  getAllSupplier: () => {
    const url = `/supplier`;
    return axiosClient.get(url);
  },
  deleteSupplier: (id) => {
    const url = `/supplier/${id}`;
    return axiosClient.delete(url);
  },
  searchSupplier: (name) => {
    const url = `/supplier/search?q=${name}`;
    return axiosClient.get(url);
  },
  createSupplier: (payload) => {
    const url = '/supplier';
    return axiosClient.post(url, payload);
  },
  patchSupplier: (supplier) => {
    const url = '/supplier';
    return axiosClient.patch(url, supplier);
  },
};

export default supplierApi;
