import queryString from 'query-string';
import { axiosClient } from '../axiosClient';

const storeDepartmentApi = {
  getDataStoreInDepartment: (data) => {
    const { id, ...query } = data;
    const queryParams = queryString.stringify(query);

    const url = `/store-department/${id}&${queryParams}`;
    return axiosClient.get(url);
  },
};

export default storeDepartmentApi;
