import { axiosClient } from '../axiosClient';
import queryString from 'query-string';
const unitApi = {
  getAllUnit: (query) => {
    const queryParams = queryString.stringify(query);
    const url = `/unit?${queryParams}`;
    return axiosClient.get(url);
  },
  getAllUnitDisabledPagination: () => {
    const url = `/unit/all`;
    return axiosClient.get(url);
  },

  deleteUnit: (id) => {
    const url = `/unit/${id}`;
    return axiosClient.delete(url);
  },
  searchSupply: (name) => {},
  createUnit: (payload) => {
    const url = '/unit';
    return axiosClient.post(url, payload);
  },
  patchSupply: (supply) => {
    const url = '/supply';
    return axiosClient.patch(url, supply);
  },
};

export default unitApi;
