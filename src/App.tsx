import React, { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import RenderRoutes, { routes } from 'routes';
import 'antd/dist/antd.min.css';
import Notification from 'app/components/Notification/Notification';
import { getLanguageFromLocalStorage, getTokenFromLocalStorage } from 'app/helper/localStorage';
import i18next from 'i18next';
import { useAppSelector } from 'store/hooks';

let checkAuth;
function App() {
  const languageUser = getLanguageFromLocalStorage();
  const { user, isAuthenticated } = useAppSelector((state) => state.auth);
  const checkAuth = !!getTokenFromLocalStorage();
  useEffect(() => {
    i18next.changeLanguage(languageUser);
  }, []);
  console.log(checkAuth);
  console.log('Auth', isAuthenticated);

  return (
    <>
      <RenderRoutes routes={routes} authenticated={checkAuth} />
      <Notification />
    </>
  );
}

export default App;
