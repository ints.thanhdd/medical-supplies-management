import HomePage from 'app/pages/HomePage/HomePage';
import LoginPage from 'app/pages/AuthPage/LoginPage';
import React, { Fragment } from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import RegisterPage from 'app/pages/AuthPage/RegisterPage';
import DefaultLayout from 'app/pages/Layouts/DefaultLayout/DefaultLayout';
import CreateSupply from 'app/pages/SupplyPage/CreateSupply';
import ExportSupplies from 'app/pages/SupplyPage/ExportSupplies';
import Supplier from 'app/pages/SupplierPage/Supplier';
import ListSupply from 'app/pages/SupplyPage/ListSupply';
import DepartmentDetail from 'app/pages/DepartmentDetail/DepartmentDetail';
import CreatePhan from 'app/pages/DepartmentDetail/Plan/CreatePlan';
import DepartmentStaff from 'app/pages/DepartmentDetail/DepartmentStaff/DepartmentStaff';
import { mPath } from 'app/constant/route';
import DepartmentDetailEdit from 'app/pages/AdmDepartmentPage/DepartmentDetailEdit';
import Department from 'app/pages/AdmDepartmentPage/Department';
import AdmUnitPage from 'app/pages/AdmUnitPage/AdmUnitPage';
import DepartmentWarehouse from 'app/pages/DepartmentWarehouse/DepartmentWarehouse';
import ListMonthlyPlan from 'app/pages/AdminPlan/ListMonthlyPlan';
import NotfoundPage from './app/pages/NotfoundPage/NotfoundPage';
import AdmWareHousePage from './app/pages/AdmWareHousePage/AdmWareHousePage';

export type RoutesProps = {
  exact?: boolean;
  path: string;
  component: React.FC<{ history: any; location: any; match: any }>;
  auth?: boolean;
  routes?: Array<RoutesProps>;
  layout?: React.FC;
};
export const routes = [
  {
    exact: true,
    path: '/addmedical',
    component: () => <div>notfound</div>,
  },
  {
    exact: true,
    path: mPath.LOGIN,
    component: () => <LoginPage />,
  },
  {
    exact: true,
    path: mPath.REGISTER,
    component: () => <RegisterPage />,
  },
  {
    path: '*',
    component: () => <Redirect to="/" />,
    routes: [
      // Users
      {
        exact: true,
        auth: true,
        layout: DefaultLayout,
        path: mPath.A_HOME,
        component: () => <HomePage />,
      },
      {
        exact: true,
        auth: true,
        layout: DefaultLayout,
        path: mPath.A_SUPPLY_CREATE,
        component: () => <CreateSupply />,
      },
      {
        exact: true,
        auth: true,
        layout: DefaultLayout,
        path: mPath.A_SUPPLY_LIST,
        component: () => <ListSupply />,
      },
      {
        exact: true,
        auth: true,
        layout: DefaultLayout,
        path: '/adm/medical/export',
        component: () => <ExportSupplies />,
      },
      {
        exact: true,
        auth: true,
        layout: DefaultLayout,
        path: mPath.A_PLAN_LIST,
        component: () => <ListMonthlyPlan />,
      },
      {
        exact: true,
        auth: true,
        layout: DefaultLayout,

        path: mPath.A_SUPPLIER,
        component: () => <Supplier />,
      },
      {
        exact: true,
        auth: true,
        layout: DefaultLayout,
        path: mPath.A_DEPARTMENTS,
        component: () => <Department />,
      },
      {
        exact: true,
        layout: DefaultLayout,
        path: mPath.A_DEPARTMENT_DETAIL,
        component: () => <DepartmentDetailEdit />,
      },
      {
        exact: true,
        layout: DefaultLayout,
        path: mPath.A_UNIT,
        component: () => <AdmUnitPage />,
      },
      {
        exact: true,
        layout: DefaultLayout,
        path: mPath.A_WAREHOUSE,
        component: () => <AdmWareHousePage />,
      },
      {
        exact: true,
        layout: DefaultLayout,
        path: mPath.M_HOME,
        component: () => <DepartmentDetail />,
      },
      {
        exact: true,
        layout: DefaultLayout,
        path: mPath.M_STAFF,
        component: () => <DepartmentStaff />,
      },
      {
        exact: true,
        layout: DefaultLayout,
        path: mPath.M_PLAN_CREATE,
        component: () => <CreatePhan />,
      },
      {
        exact: true,
        layout: DefaultLayout,
        path: mPath.M_WAREHOUSE,
        component: () => <DepartmentWarehouse />,
      },

      {
        exact: true,
        path: '*',
        component: () => <NotfoundPage />,
      },
    ],
  },
];
const RenderRoutes = ({ routes, authenticated }) => {
  return (
    <Switch>
      {routes.map((route, i) => {
        const Layout = route.layout || Fragment;
        const Component = route.component || <div />;
        if (route.auth && !authenticated) {
          return <Redirect key={i} to="/login" />;
        }
        return (
          <Route
            key={i}
            path={route.path}
            exact={route.exact}
            render={(props) => {
              return (
                <Layout>
                  {route.routes ? (
                    <RenderRoutes routes={route.routes} authenticated={authenticated} />
                  ) : (
                    <Component {...props} />
                  )}
                </Layout>
              );
            }}
          />
        );
      })}
    </Switch>
  );
};

export default RenderRoutes;
